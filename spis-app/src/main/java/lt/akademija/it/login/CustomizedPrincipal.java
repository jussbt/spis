package lt.akademija.it.login;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/*
 * Customized principal allows referring to the current user within queries
 * */
public class CustomizedPrincipal extends User {
    private int id;

    public CustomizedPrincipal(String username, String password,
                               Collection<? extends GrantedAuthority> authorities,
                               int id) {
        super(username, password, authorities);
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
