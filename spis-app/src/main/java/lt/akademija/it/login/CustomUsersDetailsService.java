package lt.akademija.it.login;

import lt.akademija.it.user.User;
import lt.akademija.it.user.UserRepository;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
@Transactional
public class CustomUsersDetailsService implements UserDetailsService {

    /* The implementation of UserDetailsService lets us
     * to look up UserDetails from database for a given username.
     * It represents an authenticated user object.
     */

    private static final Logger logger = LogManager.getLogger(CustomUsersDetailsService.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public CustomizedPrincipal loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByUserName(username).get();
        if (user == null) throw new UsernameNotFoundException(username + " not found.");
        return new CustomizedPrincipal(
                user.getUserName(),
                user.getPassword(),
                getAuthorities(user),
                user.getUserId());
    }

    private static Collection<? extends GrantedAuthority> getAuthorities(User user) {
        String[] userRoles = {user.getRole()};
        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(userRoles);
        return authorities;
    }

}