package lt.akademija.it.drug;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DrugRepository extends JpaRepository<Drug, Long> {

	Drug findByActiveIngredient(String activeIngredient);

	List<Drug> findAllByOrderByActiveIngredientAsc();

	List<Drug> findTop10ByOrderByCountDesc();

}
