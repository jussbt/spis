package lt.akademija.it.drug;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Drugs")
public class Drug implements Serializable {

	@Id
	@GeneratedValue(generator = "optimized-sequence")
	private long id;

	@Column(name = "active_ingredient", nullable = false)
	private String activeIngredient;

	@Column(columnDefinition = "int default 0")
	private int count = 0; // indicates a number of times drug with this active ingredient was purchased

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getActiveIngredient() {
		return activeIngredient;
	}

	public void setActiveIngredient(String activeIngredient) {
		this.activeIngredient = activeIngredient;
	}

}
