package lt.akademija.it.drug;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/drugs")
public class DrugController {
    @Autowired
    DrugService drugService;

    @GetMapping
    public List<Drug> getAll() {
        return drugService.getAll();
    }
    
    @GetMapping("/statistics")
	public DrugStatistics getPrescriptionStatistics() {
		return drugService.getDrugStatistics();
	}
    
}
