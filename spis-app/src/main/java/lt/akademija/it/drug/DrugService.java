package lt.akademija.it.drug;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class DrugService {
	@Autowired
	DrugRepository drugRepository;

	@Transactional
	@PreAuthorize("hasAnyRole('ROLE_DOCTOR')")
	public List<Drug> getAll() {
		return drugRepository.findAllByOrderByActiveIngredientAsc();
	}

	@Transactional
	public DrugStatistics getDrugStatistics() {
		DrugStatistics drugStatistics = new DrugStatistics();
		drugStatistics.setTop10Drugs(drugRepository.findTop10ByOrderByCountDesc());
		return drugStatistics;
	}

}
