package lt.akademija.it.drug;

import java.util.ArrayList;
import java.util.List;

public class DrugStatistics {

	private List<Drug> top10Drugs = new ArrayList<>();

	public List<Drug> getTop10Drugs() {
		return top10Drugs;
	}

	public void setTop10Drugs(List<Drug> top10Drugs) {
		this.top10Drugs = top10Drugs;
	}

}
