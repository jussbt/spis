package lt.akademija.it.user;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/")
public class UserController {

    private static final Logger logger = LogManager.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @PostMapping("new/pharmacist")
    public ResponseEntity<Void> createPharmacist(@RequestBody PharmacistDTO pharmacist) {
        return userService.createPharmacist(pharmacist);
    }

    @PostMapping("new/patient")
    public ResponseEntity<Void> createPatient(@RequestBody PatientDTO patient) {
        return userService.createPatient(patient);
    }

    @PostMapping("new/doctor")
    public ResponseEntity<Void> createDoctor(@RequestBody DoctorDTO doctor) {
        return userService.createDoctor(doctor);
    }

//For testing purposes only.
    @GetMapping("finduserById/{userId}")
    public User getUserById(@PathVariable String userId) {
        return userService.findByUserId(userId);
    }

    @GetMapping("findpatients/{personalCode}")
    public List<PatientProjection> getPatientByPersonalCode(@PathVariable String personalCode) {
        return userService.getPatientByPersonalCode(personalCode);
    }

    @PostMapping("changeDoctor/{doctorId}/{patientUuid}")
    public void changePatientDoctor(@PathVariable int doctorId, @PathVariable String patientUuid) {
        userService.changePatientDoctor(doctorId, patientUuid);
    }

    @PostMapping("finddoctor")
    public List<DoctorProjection> findDoctorByLastnameAndSpecialization(@RequestBody DoctorDTO doctorDTO) {
        return userService.findDoctorByLastNameAndSpecialization(doctorDTO);
    }

    //FOR TESTING PURPOSES ONLY .
    @DeleteMapping ("/delete/{userName}")
    public void deleteUserByUserName(@PathVariable String userName){
        userService.deleteUser(userName);
    }
}
