package lt.akademija.it.user;


import lt.akademija.it.appointment.Appointment;
import lt.akademija.it.appointment.DateFromTillDTO;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/doctor/")
public class DoctorController {
    private final static Logger logger = LogManager.getLogger(UserService.class);
    @Autowired
    DoctorService doctorService;

    @Autowired
    UserService userService;

    @GetMapping("patients")
    public Collection<PatientProjection> getDoctorPatients(Principal principal) {
        return doctorService.getDoctorPatients(principal);
    }

    //find any pacient by search parameter
    @GetMapping("findpatients/{searchParameter}")
    public List<PatientProjection> getPatientByPersonalCode(@PathVariable String searchParameter) {
        return userService.getPatientByPersonalCode(searchParameter);
    }

    //Counts All appointments for connected doctor
    @GetMapping("/appointmentscount")
    public int appointmentsCount(Principal connectedDoctor){
        return doctorService.appointmentsCount(connectedDoctor);
    }

    //returns all visits time  for connected doctor
    @GetMapping("/getvisittime")
    public String getVisitTime(Principal connectedDoctor){
        return doctorService.getVisitTime(connectedDoctor);
    }

    //Returns Visits time and appointments count grouped by year of month -2018 years
    @GetMapping("/appointmentsgroupedbymonth/{year}")
    public  List<Appointment> getAppointmentsCountAndTimeGroupedByMonth(Principal connectedDoctor,@PathVariable int year){
        return doctorService.getAppointmentsCountAndTimeGroupedByMonth(connectedDoctor,year);
    }

    @GetMapping("/appointmentsgroupedbyDay/{year}/{month}")
    public  List<Appointment> getAppointmentsCountAndTimeGroupedByDay(Principal connectedDoctor,@PathVariable int year,@PathVariable String month){
        return doctorService.getAppointmentsCountAndTimeGroupedByDay(connectedDoctor,year,month);
    }
    @PostMapping("/getvisittimeSpecific")
    public String appointmentsTimeSpecific( @RequestBody DateFromTillDTO dateDTO,Principal principal){
        return doctorService.getVisitTimeSpecific( dateDTO, principal);
    }
    @PostMapping("/getvisitcountSpecific")
    public int appointmentsCountSpecific( @RequestBody DateFromTillDTO dateDTO,Principal principal){
        return doctorService.getVisitCountSpecific( dateDTO, principal);
    }

}
