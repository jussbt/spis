package lt.akademija.it.user;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@DiscriminatorValue(value = "Doctor")
public class Doctor extends User implements Serializable {

    private String specialization;

    public Doctor() {
        super();
    }

    public Doctor(String firstName, String lastName, String userName, String password, String specialization) {
        super(firstName, lastName, userName, password);
        this.specialization = specialization;
        role = "ROLE_DOCTOR";
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

}
