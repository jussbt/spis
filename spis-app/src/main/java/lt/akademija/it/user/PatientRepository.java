package lt.akademija.it.user;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface PatientRepository extends JpaRepository<Patient, Long> {
    Collection<PatientProjection> findPatientByAssignedDoctorIdOrderByLastName(int assignedDoctorId);

    Optional<List<Patient>> findPatientByPersonalCode(String personalCode);
}
