package lt.akademija.it.user;

import lt.akademija.it.appointment.Appointment;
import lt.akademija.it.appointment.AppointmentRepository;
import lt.akademija.it.appointment.DateFromTillDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Collection;
import java.util.List;

@Service
public class DoctorService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    PatientRepository patientRepository;

    @Autowired
    AppointmentRepository appointmentRepository;

    @Transactional
    @PreAuthorize("hasRole('ROLE_DOCTOR')")
    public Collection<PatientProjection> getDoctorPatients(Principal principal) {
        String doctorConnectedUserName = principal.getName();
        User doctorConnected = userRepository.findDoctorByUserName(doctorConnectedUserName);
        return patientRepository.findPatientByAssignedDoctorIdOrderByLastName(doctorConnected.getUserId());
    }

//Returns Count of all Visits for that doctor
    @Transactional
    @PreAuthorize("hasRole('ROLE_DOCTOR')")
    public int appointmentsCount(Principal connectedDoctor){
        Doctor findDoctor = userRepository.findDoctorByUserName(connectedDoctor.getName());
        return appointmentRepository.countVisit(findDoctor.getFirstName(),findDoctor.getLastName());

    }

    //Returns Count of all Visits between that date interval.
    @Transactional
    @PreAuthorize("hasRole('ROLE_DOCTOR')")
    public int getVisitCountSpecific(@RequestBody DateFromTillDTO dateFromTillDTO, Principal connectedDoctor){
        Doctor findDoctor = userRepository.findDoctorByUserName(connectedDoctor.getName());
        return appointmentRepository.countVisitSpecific(findDoctor.getFirstName(),findDoctor.getLastName(),dateFromTillDTO.getFirstDate(),dateFromTillDTO.getSecondDate());

    }

//Returns All Visits duration - hh:mm format
    @Transactional
    @PreAuthorize("hasRole('ROLE_DOCTOR')")
    public String getVisitTime(Principal connectedDoctor){
        Doctor findDoctor = userRepository.findDoctorByUserName(connectedDoctor.getName());
        return appointmentRepository.countVisitDuration(findDoctor.getFirstName(),findDoctor.getLastName());
    }
    //Returns visit duration - hh:mm format  of all Visits between that date interval.
    @Transactional
    @PreAuthorize("hasRole('ROLE_DOCTOR')")
    public  String getVisitTimeSpecific(@RequestBody DateFromTillDTO dateFromTillDTO,Principal connectedDoctor){
        Doctor findDoctor = userRepository.findDoctorByUserName(connectedDoctor.getName());
        return appointmentRepository.countVisitDurationSpecific(findDoctor.getFirstName(),findDoctor.getLastName(),dateFromTillDTO.getFirstDate(),dateFromTillDTO.getSecondDate());
    }
    @Transactional
    @PreAuthorize("hasRole('ROLE_DOCTOR')")
    public List<Appointment> getAppointmentsCountAndTimeGroupedByMonth (Principal connectedDoctor, @PathVariable int year){
        Doctor findDoctor = userRepository.findDoctorByUserName(connectedDoctor.getName());
        return  appointmentRepository.getAppointmentsCountAndTimeGroupedByMonth(findDoctor.getFirstName(),findDoctor.getLastName(),year);
    }
    @Transactional
    @PreAuthorize("hasRole('ROLE_DOCTOR')")
    public List<Appointment> getAppointmentsCountAndTimeGroupedByDay (Principal connectedDoctor, @PathVariable int year,@PathVariable String month){
        Doctor findDoctor = userRepository.findDoctorByUserName(connectedDoctor.getName());
        return  appointmentRepository.getAppointmentsCountAndTimeGroupedByDay(findDoctor.getFirstName(),findDoctor.getLastName(),year,month);
    }





}
