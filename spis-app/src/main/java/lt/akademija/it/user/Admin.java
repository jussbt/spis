package lt.akademija.it.user;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "Admin")
public class Admin extends User {

    public Admin() {
    }

    public Admin(String firstName, String lastName, String username, String password) {
        super(firstName, lastName, username, password);
        role = "ROLE_ADMIN";
    }
} 