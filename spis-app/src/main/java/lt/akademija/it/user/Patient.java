package lt.akademija.it.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Past;
import java.io.Serializable;
import java.util.Date;

@Entity
@DiscriminatorValue(value = "Patient")

public class Patient extends User implements Serializable {

    @Past
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date birthDate;
    @Length(min = 11, max = 11)
    @Column(name = "personalCode")
    private String personalCode;
    @Column(name = "assignedDoctorId")
    private int assignedDoctorId;

    public Patient() {
        super();
    }

    public Patient(String firstName, String lastName, String userName, String password, String personalCode, Date birthDate, int doctorId) {
        super(firstName, lastName, userName, password);
        this.personalCode = personalCode;
        this.birthDate = birthDate;
        this.assignedDoctorId = doctorId;
        role = "ROLE_PATIENT";
    }

    public String getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(String personalCode) {
        this.personalCode = personalCode;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int getAssignedDoctorId() {
        return assignedDoctorId;
    }

    public void setAssignedDoctorId(int assignedDoctorId) {
        this.assignedDoctorId = assignedDoctorId;
    }
}
