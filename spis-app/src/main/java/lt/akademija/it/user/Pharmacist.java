package lt.akademija.it.user;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "Pharmacist")
public class Pharmacist extends User {

    @NotBlank
    private String workplace;

    public Pharmacist() {
        super();
    }

    public Pharmacist(String firstName, String lastName, String userName, String password, String workplace) {
        super(firstName, lastName, userName, password);
        this.workplace = workplace;
        role = "ROLE_PHARMACIST";
    }

    public String getWorkplace() {
        return workplace;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }
}
