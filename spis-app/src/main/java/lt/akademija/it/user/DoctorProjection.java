package lt.akademija.it.user;

public interface DoctorProjection {
    String getFirstname();

    String getLastname();

    String getSpecialization();

    int getUserId();
}
