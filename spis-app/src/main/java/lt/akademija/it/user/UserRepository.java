package lt.akademija.it.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findDoctorByUserId(int userId);

    Optional<Patient> findPatientByPersonalCode(String personalCode);

    Optional<User> findByUserName(String username);

    Optional<Patient> findPatientByUserName(String username);

    Patient findByUuid(String uuid);

    Patient findByUserId(int id);

    Doctor findDoctorByUserName(String userName);

    @Query("Select d.firstName as firstname, d.lastName as lastname, d.specialization as specialization, d.userId as userId" +
            " from Doctor d where d.lastName like ?1 and d.specialization like ?2%")
    List<DoctorProjection> findDoctorByLastNameAndSpecialization(String lastName, String specialization);

    @Query("select p.personalCode as personalCode, p.uuid as uuid, p.firstName as firstName " +
            ", p.lastName as lastName, p.birthDate as birthDate, p.assignedDoctorId as assignedDoctorId" +
            " from Patient p where p.personalCode = ?1")
    List<PatientProjection> findPatientForAdminByPersonalCode(String personalCode);


}
