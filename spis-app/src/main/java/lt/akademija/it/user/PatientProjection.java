package lt.akademija.it.user;

import java.util.Date;

public interface PatientProjection {

    String getPersonalCode();

    String getUuid();

    String getFirstName();

    String getLastName();

    Date getBirthDate();

    int getAssignedDoctorId();
}
