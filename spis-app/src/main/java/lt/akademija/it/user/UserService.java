package lt.akademija.it.user;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final static Logger logger = LogManager.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private PatientRepository patientRepository;

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Void> createPharmacist(@RequestBody PharmacistDTO pharmacist) {
        String password = passwordEncoder.encode(pharmacist.getPassword());
        Pharmacist newPharmacist = new Pharmacist(pharmacist.getFirstName(), pharmacist.getLastName(),
                pharmacist.getUserName(), password, pharmacist.getWorkplace());
        return this.getUserPersistResult(newPharmacist);
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Void> createPatient(@RequestBody PatientDTO patient) {
        int doctorId = patient.getAssignedDoctorId();
        User doctor = userRepository.findDoctorByUserId(doctorId);
        boolean isDoctor = false;
        if (doctor != null) {
            isDoctor = doctor.getClass().getSimpleName().equals("Doctor");
        }
        if ((doctorId != 0 && isDoctor) || doctorId == 0) {
            String password = passwordEncoder.encode(patient.getPassword());
            Patient newPatient = new Patient(patient.getFirstName(), patient.getLastName(), patient.getUserName(),
                    password, patient.getPersonalCode(), patient.getBirthDate(), doctorId);
            return this.getPatientPersistResult(newPatient);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Void> createDoctor(@RequestBody DoctorDTO doctor) {
        String password = passwordEncoder.encode(doctor.getPassword());
        Doctor newDoctor = new Doctor(doctor.getFirstName(), doctor.getLastName(), doctor.getUserName(),
                password, doctor.getSpecialization());
        return this.getUserPersistResult(newDoctor);
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public User findByUserId(@PathVariable String userId) {
        return userRepository.findDoctorByUserId(Integer.parseInt(userId));
    }

    @Transactional
    @PreAuthorize("hasAnyRole('ROLE_ADMIN,ROLE_DOCTOR')")
    public List<PatientProjection> getPatientByPersonalCode(@PathVariable String searchParameter) {
        return userRepository.findPatientForAdminByPersonalCode(searchParameter);
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void changePatientDoctor(@PathVariable int doctorId, @PathVariable String  patientUuid) {
        User doctor = userRepository.findDoctorByUserId(doctorId);
        Patient patient = userRepository.findByUuid(patientUuid);
        patient.setAssignedDoctorId(doctor.getUserId());
        logger.info("Patient '" + patient.getUserName() + "' has been assigned to doctor '" +
        doctor.getUserName() + "'.");
        userRepository.save(patient);
    }

    // returns 201 if entity has been persisted to DB, 409 otherwise
    private ResponseEntity<Void> getUserPersistResult(User userToPersist) {
        Optional<User> user = userRepository.findByUserName(userToPersist.getUserName());
        if (user.isPresent()) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } else {
            userRepository.save(userToPersist);
            logger.info("New " + userToPersist.getClass().getSimpleName().toLowerCase() +
            " '" + userToPersist.getUserName() + "' has been created.");
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
    }

    // returns 201 if entity has been persisted to DB.
    private ResponseEntity<Void> getPatientPersistResult(Patient userToPersist) {
        Optional<List<Patient>> patientByPersonalId = patientRepository.findPatientByPersonalCode(userToPersist.getPersonalCode());
        Optional<User> userByUsername = userRepository.findByUserName(userToPersist.getUserName());
        if (patientByPersonalId.isPresent()) {
            return new ResponseEntity<>(HttpStatus.FOUND);
        } else if (userByUsername.isPresent()) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } else {
            userRepository.save(userToPersist);
            logger.info("New " + userToPersist.getClass().getSimpleName().toLowerCase() + " " +
                    "'" + userToPersist.getUserName() + "' has been created.");
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
    }

    //Finds And returns Doctor by lastNameAndSpecialization
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<DoctorProjection> findDoctorByLastNameAndSpecialization(@RequestBody DoctorDTO doctorDTO) {
        return userRepository.findDoctorByLastNameAndSpecialization(doctorDTO.getLastName(), doctorDTO.getSpecialization());
    }

    //For testing purposes
    @Transactional
    public void deleteUser(@PathVariable String userName){
        userRepository.delete(userRepository.findByUserName(userName).get());
    }
}
