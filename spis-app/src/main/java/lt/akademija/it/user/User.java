package lt.akademija.it.user;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "USERS",
        indexes = {
                @Index(name = "idx_iso_code", columnList = "lastName", unique = false), // we need to find doctor by lastname
                @Index(name = "idx_personalId", columnList = "personalCode"),//we need this  to fast find patient with specific personal Code
                @Index(name = "idx_assignedDoctorId", columnList = "assignedDoctorId"),//we need this index to fast find all assigned patients
                @Index(name = "idx_role", columnList = "role"),//We need to find Fast if user exist by personalID/username
                @Index(name = "idx_uuid", columnList = "uuid")
        }
)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "USER_TYPE")

public abstract class User implements Serializable {

    @Id
    @GeneratedValue(generator = "optimized-sequence")
    private int userId;
    @NotBlank
    @Length(max = 20)
    private String firstName;
    @NotBlank
    @Length(max = 20)
    @Column(name = "lastName")
    private String lastName;
    @NotBlank
    @Length(max = 20)
    @Column(unique = true)
    private String userName;
    @NotBlank
    private String password;
    @NotBlank
    protected String role;
    private String uuid;

    @PrePersist
    public void createUuid() {
        if (this.uuid == null) {
            this.uuid = UUID.randomUUID().toString();
        }
    }

    public User() {
    }

    public User(String firstName, String lastName, String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = username;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
