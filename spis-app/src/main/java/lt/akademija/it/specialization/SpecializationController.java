package lt.akademija.it.specialization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/specialization")
public class SpecializationController {
    @Autowired
    private SpecializationServices specializationServices;

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<Specialization> specializationList() {
        return specializationServices.getAll();
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public void addSpecialization(@RequestBody SpecializationDTO spec) {
        specializationServices.addSpecialization(spec);
    }

    //For testing .
    @DeleteMapping("/delete/{name}")
    @ResponseStatus(HttpStatus.FOUND)
    public void deleleteByName(@PathVariable String name){
        specializationServices.delete(name);
    }
}
