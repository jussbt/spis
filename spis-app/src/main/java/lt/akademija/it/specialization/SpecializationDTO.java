package lt.akademija.it.specialization;


public class SpecializationDTO {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
