package lt.akademija.it.specialization;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Specializations")
public class Specialization {

    @Id
    @GeneratedValue(generator = "optimized-sequence")
    private long id;

    @Column(unique = true)
    @NotNull
    private String name;

    public Specialization() {
    }

    public Specialization(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
