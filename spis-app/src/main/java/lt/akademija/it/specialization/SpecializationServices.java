package lt.akademija.it.specialization;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class SpecializationServices {
    private final static Logger logger = LogManager.getLogger(SpecializationServices.class);

    @Autowired
    SpecializationRepository specializationRepository;

    @Transactional
    public List<Specialization> getAll() {
        return specializationRepository.findAllByOrderByNameAsc();
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void addSpecialization(@RequestBody SpecializationDTO specName) {
        Specialization specialization = new Specialization(specName.getName());
        logger.info("New specialization '" + specialization.getName() + "' has been created.");
        specializationRepository.save(specialization);
    }

    //for testing purposes. UI doesnt have this service.
    @Transactional
    public void delete(@PathVariable String name){
        Specialization specialization = specializationRepository.findByName(name);
        specializationRepository.delete(specialization.getId());
        logger.warn("Specialization : " +specialization.getName()+" was deleted");
    }
}
