package lt.akademija.it.prescription;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Drug {

	@Column(name = "active_ingredient", nullable = false)
	private String activeIngredient;

	public String getActiveIngredient() {
		return activeIngredient;
	}

	public void setActiveIngredient(String activeIngredient) {
		this.activeIngredient = activeIngredient;
	}

}