package lt.akademija.it.prescription;

import lt.akademija.it.user.Patient;
import lt.akademija.it.user.User;
import lt.akademija.it.user.UserRepository;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.Optional;

@Service
public class PrescriptionServices {

    private final static Logger logger = LogManager.getLogger(PrescriptionServices.class);

    @Autowired
    PrescriptionRepository prescriptionRepository;

    @Autowired
    UserRepository userRepository;

    @Transactional
    @PreAuthorize("hasAnyRole('ROLE_PHARMACIST,ROLE_PATIENT,ROLE_DOCTOR')")
    public Page<PrescriptionProjection> validPatientPrescriptions(int page, Principal principal) {
        Optional<Patient> patient = userRepository.findPatientByUserName(principal.getName());
        if (patient.isPresent()) {
            int patientId = patient.get().getUserId();
            return prescriptionRepository.findValidPatientPrescriptions(patientId, new PageRequest(page, 10));
        } else {
            return null;
        }
    }

    @Transactional
    @PreAuthorize("hasAnyRole('ROLE_PHARMACIST,ROLE_PATIENT,ROLE_DOCTOR')")
    public Page<PrescriptionProjection> invalidPatientPrescriptions(int page, Principal principal) {
        Optional<Patient> patient = userRepository.findPatientByUserName(principal.getName());
        if (patient.isPresent()) {
            int patientId = patient.get().getUserId();
            return prescriptionRepository.findInvalidPatientPrescriptions(patientId, new PageRequest(page, 10));
        } else {
            return null;
        }
    }

    @Transactional
    @PreAuthorize("hasAnyRole('ROLE_PHARMACIST,ROLE_PATIENT,ROLE_DOCTOR')")
    public Page<PrescriptionProjection> doctorPatientPrescriptions(String id, int page) {
        return prescriptionRepository.findDoctorPatientPrescriptions(id, new PageRequest(page, 10));
    }

    @Transactional
    @PreAuthorize("hasAnyRole('ROLE_PHARMACIST,ROLE_PATIENT,ROLE_DOCTOR')")
    public void create(@RequestBody PrescriptionDTO prescriptionDTO, Principal principal) {
        lt.akademija.it.prescription.Drug drugToPassCreatingPrescription = new lt.akademija.it.prescription.Drug();
        drugToPassCreatingPrescription.setActiveIngredient(prescriptionDTO.getDrugMaterial());

        User findPatient = userRepository.findByUuid(prescriptionDTO.getPatientUuid());

        Prescription newPrescription = new Prescription(drugToPassCreatingPrescription, prescriptionDTO.getDescription(),
                prescriptionDTO.getAmount(),
                prescriptionDTO.getUnits(),
                findPatient,
                prescriptionDTO.getExpirationDate());
        prescriptionRepository.save(newPrescription);
        logger.info("'" + principal.getName() + "' registered prescription for '" + findPatient.getUserName() + "'.");
    }

    @Transactional
    @PreAuthorize("hasAnyRole('ROLE_PHARMACIST,ROLE_PATIENT,ROLE_DOCTOR')")
    public Page<PrescriptionProjection> getPrescriptions(String personalCode, int page, HttpServletResponse response) {
        Optional<Patient> patient = userRepository.findPatientByPersonalCode(personalCode);
        if (patient.isPresent()) {
            Page<PrescriptionProjection> prescriptionList = prescriptionRepository.findValidPatientPrescriptionsForPharmacist(
                    patient.get().getUserId(), new PageRequest(page, 10)
            );
            return prescriptionList;
        } else {
            response.setStatus(404);
            return null;
        }
    }

    @Transactional
    public void deletePrescription(@PathVariable String  units){
      long id=  prescriptionRepository.findByUnits(units);
      prescriptionRepository.delete(id);
    }
}
