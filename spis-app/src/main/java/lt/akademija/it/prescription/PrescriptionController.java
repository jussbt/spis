package lt.akademija.it.prescription;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@RestController
@RequestMapping(value = "/api/prescriptions")
public class PrescriptionController {
	@Autowired
	PrescriptionServices prescriptionServices;

	@GetMapping(value = "/valid", params = { "page" })
	public Page<PrescriptionProjection> validPatientPrescriptions(@RequestParam("page") int page,
			Principal connectedPatient) {
		return prescriptionServices.validPatientPrescriptions(page, connectedPatient);
	}

	@GetMapping(value = "/invalid", params = { "page" })
	public Page<PrescriptionProjection> invalidPatientPrescriptions(@RequestParam("page") int page,
			Principal connectedPatient) {
		return prescriptionServices.invalidPatientPrescriptions(page, connectedPatient);
	}

	@GetMapping(value = "/{id}", params = { "page" })
	public Page<PrescriptionProjection> doctorPatientAppointments(@PathVariable String id,
			@RequestParam("page") int page) {
		return prescriptionServices.doctorPatientPrescriptions(id, page);
	}

	@PostMapping("/create")
	@ResponseStatus(HttpStatus.CREATED)
	public void createPrescription(@RequestBody PrescriptionDTO prescriptionDTO, Principal principal) {
		prescriptionServices.create(prescriptionDTO, principal);
	}

	@GetMapping(value = "/pharmacist/{personalCode}", params = { "page" })
	public Page<PrescriptionProjection> getPrescriptions(@PathVariable String personalCode,
														 @RequestParam("page") int page, HttpServletResponse response) {
		return prescriptionServices.getPrescriptions(personalCode, page, response);
	}
	//I need this method for integration testing - I do not want to save objects created by IT into database;
	//WE DO NOT USE THIS CONTROLLER EXCEPT FOR TESTING PURPOSES.
	@DeleteMapping("/{units}")
	public void deletePrescription(@PathVariable String units){
		prescriptionServices.deletePrescription(units);
	}

}
