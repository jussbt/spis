package lt.akademija.it.prescription;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PrescriptionRepository extends JpaRepository<Prescription, Long> {
    @Query("SELECT p.amount as amount, p.date as date, " +
            "p.description as description, p.expiryDate as expiryDate, " +
            "p.uuid as uuid, p.units as units, p.drug as drug" +
            " FROM Prescription p " +
            "WHERE p.patient.userId = ?1 AND (p.expiryDate >= CURRENT_DATE OR p.expiryDate IS NULL)")
    Page<PrescriptionProjection> findValidPatientPrescriptions(int id, Pageable pageable);

    @Query("SELECT p.amount as amount, p.date as date, " +
            "p.description as description, p.expiryDate as expiryDate, " +
            "p.uuid as uuid, p.units as units, p.drug as drug" +
            " FROM Prescription p " +
            "WHERE p.patient.userId = ?1 AND p.expiryDate < CURRENT_DATE")
    Page<PrescriptionProjection> findInvalidPatientPrescriptions(int id, Pageable pageable);

    @Query("SELECT p.amount as amount, p.date as date, " +
            "p.description as description, p.expiryDate as expiryDate, " +
            "p.uuid as uuid, p.units as units, p.drug as drug" +
            " FROM Prescription p " +
            "WHERE p.patient.uuid = ?1")
    Page<PrescriptionProjection> findDoctorPatientPrescriptions(String uuid, Pageable pageable);

    @Query("SELECT p.amount as amount, p.date as date, " +
            "p.description as description, p.expiryDate as expiryDate, " +
            "p.uuid as uuid, p.units as units, p.drug as drug" +
            " FROM Prescription p " +
            "WHERE p.patient.userId = ?1 AND (p.expiryDate >= CURRENT_DATE OR p.expiryDate IS NULL) " +
            "order by date desc")
    Page<PrescriptionProjection> findValidPatientPrescriptionsForPharmacist(int per, Pageable pageable);

    Optional<Prescription> findByUuid(String uuid);

    @Query("select p.id from Prescription  p where p.units =?1")
    long findByUnits(String Units);
}
