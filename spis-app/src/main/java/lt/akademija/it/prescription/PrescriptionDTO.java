package lt.akademija.it.prescription;

import java.util.Date;

public class PrescriptionDTO {

    private Date expirationDate;
    private String drugMaterial;
    private double amount;
    private String units;
    private String Description;
    private String patientUuid;

    public String getPatientUuid() {
        return patientUuid;
    }

    public void setPatientUuid(String patientUuid) {
        this.patientUuid = patientUuid;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getDrugMaterial() {
        return drugMaterial;
    }

    public void setDrugMaterial(String drugMaterial) {
        this.drugMaterial = drugMaterial;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
