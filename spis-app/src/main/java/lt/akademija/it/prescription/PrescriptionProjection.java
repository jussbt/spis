package lt.akademija.it.prescription;

import java.util.Date;

public interface PrescriptionProjection {
    double getAmount();

    Date getDate();

    String getDescription();

    DrugProjection getDrug();

    Date getExpiryDate();

    String getUuid();

    String getUnits();

    interface DrugProjection {
        String getActiveIngredient();
    }
}
