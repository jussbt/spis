package lt.akademija.it.prescription;

import com.fasterxml.jackson.annotation.JsonFormat;
import lt.akademija.it.user.Patient;
import lt.akademija.it.user.User;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Target;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "Prescriptions",
        indexes = {
                @Index(name = "idx_prescriptionUuid", columnList = "uuid"),
                @Index(name = "idx_prescriptionExpirationDate", columnList = "expiry_date")
        })
public class Prescription implements Serializable {

    @Id
    @GeneratedValue(generator = "optimized-sequence")
    private long id;

    @Column(name = "prescription_date")
    @Temporal(TemporalType.DATE)
    @CreationTimestamp
    private Date date;

    @Embedded
    private Drug drug;

    @Column
    private String description;

    @Column(name = "amount_in_dose", nullable = false)
    private double amount;

    @Column(name = "measurement_units", nullable = false)
    private String units;

    @Target(Patient.class)
    @ManyToOne(fetch = FetchType.LAZY)
    private User patient;

    @Column(name = "expiry_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date expiryDate;

    private String uuid;

    @PrePersist
    public void createUuid() {
        if (this.uuid == null) {
            this.uuid = UUID.randomUUID().toString();
        }
    }

    public Prescription() {
    }

    public Prescription(Drug drug, String description, double amount, String units, User patient, Date expiryDate) {
        this.drug = drug;
        this.description = description;
        this.amount = amount;
        this.units = units;
        this.patient = patient;
        this.expiryDate = expiryDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Drug getDrug() {
        return drug;
    }

    public void setDrug(Drug drug) {
        this.drug = drug;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getPatient() {
        return patient;
    }

    public void setPatient(User patient) {
        this.patient = patient;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public void setId(long id) {
        this.id = id;
    }
}
