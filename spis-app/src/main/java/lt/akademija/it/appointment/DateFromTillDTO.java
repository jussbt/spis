package lt.akademija.it.appointment;

import java.util.Date;

public class DateFromTillDTO {

    private Date firstDate;


    private Date secondDate;

    public Date getFirstDate() {
        return firstDate;
    }

    public void setFirstDate(Date firstDate) {
        this.firstDate = firstDate;
    }

    public Date getSecondDate() {
        return secondDate;
    }

    public void setSecondDate(Date secondDate) {
        this.secondDate = secondDate;
    }
}
