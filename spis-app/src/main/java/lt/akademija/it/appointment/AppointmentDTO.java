package lt.akademija.it.appointment;

public class AppointmentDTO {

    private String description;
    private int duration;
    private boolean isCompensated;
    private boolean isRepeated;
    private Long diseaseId;
    private String patientUuid;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isCompensated() {
        return isCompensated;
    }

    public void setCompensated(boolean compensated) {
        isCompensated = compensated;
    }

    public boolean isRepeated() {
        return isRepeated;
    }

    public void setRepeated(boolean repeated) {
        isRepeated = repeated;
    }

    public Long getDiseaseId() {
        return diseaseId;
    }

    public void setDiseaseId(Long diseaseId) {
        this.diseaseId = diseaseId;
    }

    public String getPatientUuid() {
        return patientUuid;
    }

    public void setPatientUuid(String patientUuid) {
        this.patientUuid = patientUuid;
    }
}
