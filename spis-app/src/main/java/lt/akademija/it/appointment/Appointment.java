package lt.akademija.it.appointment;

import com.fasterxml.jackson.annotation.JsonFormat;
import lt.akademija.it.disease.Disease;
import lt.akademija.it.user.Patient;
import lt.akademija.it.user.User;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Target;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "Appointments",
        indexes = {
                @Index(name = "idx_patientId", columnList = "patient_id"),
                @Index(name = "idx_date", columnList = "appointment_date"),
                @Index(name = "idx_doctorFirstName",columnList ="doctor_firstname" ),
                @Index(name = "idx_doctorLastName",columnList ="doctor_lastName" )
        })
public class Appointment implements Serializable {

    @Id
    @GeneratedValue(generator = "optimized-sequence")
    private long id;

    @Column(name = "app_duration", nullable = false)
    @JsonFormat(pattern = "HH:mm")
    private int duration;

    @ManyToOne
    @JoinColumn(name = "disease_id", foreignKey = @ForeignKey(name = "DISEASE_ID_FK"))
    private Disease disease;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patient_id", foreignKey = @ForeignKey(name = "PATIENT_ID_FK"))
    @Target(Patient.class)
    private User patient;

    private String description;

    @NotNull
    private boolean isCompensated;

    @Embedded
    private Doctor doctor;

    @Column(name = "appointment_date")
    @Temporal(TemporalType.DATE)
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date date;

    @NotNull
    private boolean isRepeated;

    public Appointment() {
    }

    public Appointment(int duration, Disease disease, User patient, String description, boolean isCompensated, Doctor doctor, boolean isRepeated) {
        this.duration = duration;
        this.disease = disease;
        this.patient = patient;
        this.description = description;
        this.isCompensated = isCompensated;
        this.doctor = doctor;
        this.isRepeated = isRepeated;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Disease getDisease() {
        return disease;
    }

    public void setDisease(Disease disease) {
        this.disease = disease;
    }

    public User getPatient() {
        return patient;
    }

    public void setPatient(User patient) {
        this.patient = patient;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCompensated() {
        return isCompensated;
    }

    public void setCompensated(boolean compensated) {
        isCompensated = compensated;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isRepeated() {
        return isRepeated;
    }


    public void setRepeated(boolean repeated) {
        isRepeated = repeated;
    }

}
