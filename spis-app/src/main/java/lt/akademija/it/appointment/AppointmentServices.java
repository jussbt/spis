package lt.akademija.it.appointment;

import lt.akademija.it.disease.Disease;
import lt.akademija.it.disease.DiseaseRepository;
import lt.akademija.it.user.Doctor;
import lt.akademija.it.user.Patient;
import lt.akademija.it.user.UserRepository;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.security.Principal;

@Service
public class AppointmentServices {

    private static final Logger logger = LogManager.getLogger(AppointmentServices.class);

    @Autowired
    AppointmentRepository appointmentRepository;
    @Autowired
    DiseaseRepository diseaseRepository;
    @Autowired
    UserRepository userRepository;

    @Transactional
    @PreAuthorize("hasAnyRole('ROLE_DOCTOR,ROLE_PATIENT,ROLE_PHARMACIST')")
    public Page<AppointmentProjection> patientAppointments(int page) {
        return appointmentRepository.findPatientAppointments(new PageRequest(page, 10));
    }

    @Transactional
    @PreAuthorize("hasAnyRole('ROLE_DOCTOR,ROLE_PATIENT,ROLE_PHARMACIST')")
    public Page<AppointmentProjection> doctorPatientAppointments(String id, int page) {
        return appointmentRepository.findDoctorPatientAppointments(id, new PageRequest(page, 10));
    }

    @Transactional
    @PreAuthorize("hasAnyRole('ROLE_DOCTOR')")
    public void createAppointment(@RequestBody AppointmentDTO appointmentDTO, Principal connectedDoctor) {
        Disease findDisease = diseaseRepository.findById(appointmentDTO.getDiseaseId());
        Patient findPatient = userRepository.findByUuid(appointmentDTO.getPatientUuid());
        Doctor findDoctor = userRepository.findDoctorByUserName(connectedDoctor.getName());
        lt.akademija.it.appointment.Doctor doctor = new lt.akademija.it.appointment.Doctor();
        doctor.setFirstname(findDoctor.getFirstName());
        doctor.setLastname(findDoctor.getLastName());
        Appointment newAppointment = new Appointment(appointmentDTO.getDuration(), findDisease, findPatient, appointmentDTO.getDescription(),
                appointmentDTO.isCompensated(), doctor, appointmentDTO.isRepeated());
        logger.info("'" + findDoctor.getUserName() + "' registered appointment for '" + findPatient.getUserName() + "'.");
        appointmentRepository.save(newAppointment);

        // Disease entitie's count should only be update if appointment is not repeated
        if (!appointmentDTO.isRepeated()) {
            findDisease.setCount(findDisease.getCount() + 1);
            diseaseRepository.save(findDisease);
        }
    }




}