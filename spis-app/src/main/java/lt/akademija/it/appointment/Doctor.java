package lt.akademija.it.appointment;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Doctor {

    @Column(name = "doctor_firstname", nullable = false)
    private String firstname;

    @Column(name = "doctor_lastname", nullable = false)
    private String lastname;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
