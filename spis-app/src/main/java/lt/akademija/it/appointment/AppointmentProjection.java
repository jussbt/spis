package lt.akademija.it.appointment;

import java.util.Date;

/*Projection is used for patient's panel*/
public interface AppointmentProjection {
    boolean getIsCompensated();
    boolean getIsRepeated();

    Date getDate();

    String getDescription();

    int getDuration();

    DoctorProjection getDoctor();

    DiseaseProjection getDisease();

    interface DiseaseProjection {
        String getDescription();

        String getCode();
    }

    interface DoctorProjection {
        String getFirstname();

        String getLastname();
    }
}
