package lt.akademija.it.appointment;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

    @Query("Select  a.isRepeated as isRepeated ,a.isCompensated as isCompensated, a.date as date, a.description as description, " +
            "a.duration as duration, a.isRepeated as isRepeated, a.doctor as doctor, a.disease as disease " +
            "from Appointment a where a.patient.userName = ?#{ principal?.username } order by a.date desc")
    Page<AppointmentProjection> findPatientAppointments(Pageable pageable);

    @Query("Select a.isCompensated as isCompensated, a.date as date, a.description as description, " +
            "a.duration as duration, a.isRepeated as isRepeated, a.doctor as doctor, a.disease as disease " +
            "from Appointment a where a.patient.uuid = ?1 order by a.date desc")
    Page<AppointmentProjection> findDoctorPatientAppointments(String uuid, Pageable pageable);


    @Query("Select COUNT (a.id) as  getTotalVisits from Appointment  a where a.doctor.firstname=?1 and a.doctor.lastname=?2")
    int countVisit(String  doctorFirstName,String doctorLastName);

    @Query("Select COUNT (a.id) as  getTotalVisits from Appointment  a where a.doctor.firstname=?1 and a.doctor.lastname=?2 and  a.date between ?3 and ?4")
    int countVisitSpecific(String  doctorFirstName,String doctorLastName,Date date1, Date date2);

    @Query("Select concat(sum(a.duration)/60,' val ',sum (a.duration)%60,' min' ) from Appointment  a where a.doctor.firstname=?1 and a.doctor.lastname=?2")
    String countVisitDuration(String doctorFirstName,String doctorLastName);

    @Query("Select concat(sum(a.duration)/60,' val ',sum (a.duration)%60,' min ' ) from Appointment  a where a.doctor.firstname=?1 and a.doctor.lastname=?2 and a.date between ?3 and ?4")
    String countVisitDurationSpecific(String doctorFirstName, String doctorLastName, Date date1, Date date2);


  // Long and Difficult query .

    @Query("Select MONTHNAME(a.date) as menuo, count(a.id) ,concat(sum(a.duration)/60,' val ',sum (a.duration)%60,' min ' ),CASE \n" +
            "WHEN monthname(APPOINTMENT_DATE )  ='January' then 'Sausis'\n" +
            "WHEN monthname(APPOINTMENT_DATE )  ='February' then 'Vasaris'\n" +
            "WHEN monthname(APPOINTMENT_DATE )  ='March' then 'Kovas'\n" +
            "WHEN monthname(APPOINTMENT_DATE )  ='April' then 'Balandis'\n" +
            "WHEN monthname(APPOINTMENT_DATE )  ='May' then 'Gegužis'\n" +
            "WHEN monthname(APPOINTMENT_DATE )  ='June' then 'Birželis'\n" +
            "WHEN monthname(APPOINTMENT_DATE )  ='July' then 'Liepa'\n" +
            "WHEN monthname(APPOINTMENT_DATE )  ='August' then 'Rugpjūtis'\n" +
            "WHEN monthname(APPOINTMENT_DATE )  ='September' then 'Rugsėjis'\n" +
            "WHEN monthname(APPOINTMENT_DATE )  ='October' then 'Spalis'\n" +
            "WHEN monthname(APPOINTMENT_DATE )  ='November' then 'Lapkritis'\n" +
            "WHEN monthname(APPOINTMENT_DATE )  ='December' then 'Gruodis'\n" +
            "End  as menesiai from Appointment  a  where Year(a.date)=?3 and  a.doctor.firstname=?1 and a.doctor.lastname=?2 group by col_3_0_,col_0_0_  order by count(a.id) desc" )
    List <Appointment> getAppointmentsCountAndTimeGroupedByMonth(String doctorFirstName,String doctorLastName,int year);


    @Query("Select Day(a.date) as diena, count(a.id) ,concat(sum(a.duration)/60,' val ',sum (a.duration)%60,'min' ), concat(Day(a.date),' diena')  from Appointment  a  where Year(a.date)=?3 and MONTHNAME(a.date)=?4 and  a.doctor.firstname=?1 and a.doctor.lastname=?2 group by Day(a.date) order by col_0_0_ asc" )
    List <Appointment> getAppointmentsCountAndTimeGroupedByDay(String doctorFirstName,String doctorLastName,int year,String month);



}