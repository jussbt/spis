package lt.akademija.it.appointment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping(value = "/api/appointment")
public class AppointmentController {
    @Autowired
    private AppointmentServices appointmentServices;

    @GetMapping(params = {"page"})
    public Page<AppointmentProjection> patientAppointments(@RequestParam("page") int page) {
        return appointmentServices.patientAppointments(page);
    }

    @GetMapping(value = "/{id}", params = {"page"})
    public Page<AppointmentProjection> doctorPatientAppointments(@PathVariable String id, @RequestParam("page") int page) {
        return appointmentServices.doctorPatientAppointments(id, page);
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.OK)
    public void createAppointment(@RequestBody AppointmentDTO appointmentDTO, Principal connectedDoctor) {
        appointmentServices.createAppointment(appointmentDTO, connectedDoctor);
    }
}
