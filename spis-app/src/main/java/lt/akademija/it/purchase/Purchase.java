package lt.akademija.it.purchase;

import lt.akademija.it.prescription.Prescription;
import lt.akademija.it.user.Pharmacist;
import lt.akademija.it.user.User;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Target;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "prescription_purchases")
public class Purchase implements Serializable {
    public Purchase() {
    }

    @Id
    @GeneratedValue(generator = "optimized-sequence")
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "prescription_id")
    private Prescription prescription;

    @Column(name = "purchase_date")
    @Temporal(TemporalType.DATE)
    @CreationTimestamp
    private Date date;

    @Target(Pharmacist.class)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pharmacist_id", foreignKey = @ForeignKey(name = "PHARMACIST_ID_FK"))
    private User pharmacist;

    public void setPrescription(Prescription prescription) {
        this.prescription = prescription;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Prescription getPrescription() {
        return prescription;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getPharmacist() {
        return pharmacist;
    }

    public void setPharmacist(User pharmacist) {
        this.pharmacist = pharmacist;
    }
}
