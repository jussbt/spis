package lt.akademija.it.purchase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/purchase/")
public class PurchaceController {


    @Autowired
    PurchaceService purchaceService;

    @GetMapping("{prescriptionUuid}")
    public List<PurchaseProjection> getPurchases(@PathVariable String prescriptionUuid) {
        return purchaceService.getPurchases(prescriptionUuid);
    }

    @PostMapping("{prescriptionUuid}")
    @ResponseStatus(HttpStatus.OK)
    public void savePurchace(@PathVariable String prescriptionUuid, Principal connectedPharmacist) {
        purchaceService.savePurchace(prescriptionUuid, connectedPharmacist);
    }

}
