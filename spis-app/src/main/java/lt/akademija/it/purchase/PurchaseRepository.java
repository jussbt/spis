package lt.akademija.it.purchase;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, Long> {

    @Query("Select p.date as date, p.pharmacist as pharmacist from Purchase p where p.prescription.uuid = ?1 order by p.date desc")
    List<PurchaseProjection> findAllByPrescriptionUuid(String uuid);
}
