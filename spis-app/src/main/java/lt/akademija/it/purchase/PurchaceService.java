package lt.akademija.it.purchase;

import lt.akademija.it.drug.Drug;
import lt.akademija.it.drug.DrugRepository;
import lt.akademija.it.prescription.Prescription;
import lt.akademija.it.prescription.PrescriptionRepository;
import lt.akademija.it.user.UserRepository;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Service
public class PurchaceService {
    private final static Logger logger = LogManager.getLogger(PurchaceService.class);
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PrescriptionRepository prescriptionRepository;
    @Autowired
    private PurchaseRepository purchaceRepository;
    @Autowired
    private DrugRepository drugRepository;

    @Transactional
    @PreAuthorize("hasRole('ROLE_PHARMACIST')")
    public void savePurchace(@PathVariable String prescriptionUuid, Principal connectedPharmacist) {
        String username = connectedPharmacist.getName();
        Optional<lt.akademija.it.user.User> user = userRepository.findByUserName(username);
        if (user.isPresent()) {
            Optional<Prescription> prescription = prescriptionRepository.findByUuid(prescriptionUuid);
            if (prescription.isPresent()) {
                Purchase purchase = new Purchase();
                purchase.setPharmacist(user.get());
                purchase.setPrescription(prescription.get());
                Drug drug = drugRepository.findByActiveIngredient(prescription.get().getDrug().getActiveIngredient());
                drug.setCount(drug.getCount()+1);
                drugRepository.save(drug);
                purchaceRepository.save(purchase);
                logger.info("'" + username + "' added a new purchase for '" + user.get().getUserName() +
                "' prescription ID " + prescription.get().getId() + ".");
            }
        }
    }

    @Transactional
    @PreAuthorize("hasAnyRole('ROLE_PHARMACIST,ROLE_PATIENT,ROLE_DOCTOR')")
    public List<PurchaseProjection> getPurchases(String prescriptionUuid) {
        return purchaceRepository.findAllByPrescriptionUuid(prescriptionUuid);
    }
}
