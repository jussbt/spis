package lt.akademija.it.purchase;

import java.util.Date;

public interface PurchaseProjection {
    Date getDate();

    PharmacistProjection getPharmacist();

    interface PharmacistProjection {
        String getWorkplace();
    }
}
