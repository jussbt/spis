package lt.akademija.it.password;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/api/changepassword")
public class PasswordChangerController {

    @Autowired
    private PasswordChangerService passwordChangerService;


    @PutMapping
    public ResponseEntity<Void> changePassword(@RequestBody PasswordDTO passwordDTO, Principal principal) {
        return passwordChangerService.changePassword(passwordDTO, principal);
    }
}
