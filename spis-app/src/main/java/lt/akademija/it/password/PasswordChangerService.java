package lt.akademija.it.password;


import lt.akademija.it.user.User;
import lt.akademija.it.user.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import java.security.Principal;

@Service
public class PasswordChangerService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private static Logger logger = LogManager.getLogger(PasswordChangerService.class);

    @Transactional
    @PreAuthorize("hasAnyRole('ROLE_PHARMACIST,ROLE_PATIENT,ROLE_DOCTOR,ROLE_ADMIN')")
    public ResponseEntity<Void> changePassword(@RequestBody PasswordDTO passwordDTO, Principal principal) {
        User user = validateUser(principal); // user who is trying to change a password
        String dbEncryptedPassword = user.getPassword();
        if (passwordEncoder.matches(passwordDTO.getOldPassword(), dbEncryptedPassword)) {
            String newPasswordEncrypted = passwordEncoder.encode(passwordDTO.getNewPassword());
            user.setPassword(newPasswordEncrypted);
            userRepository.save(user);
            logger.info("'" + user.getUserName() + "' has changed password.");
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    private User validateUser(Principal principal) {
        String userId = principal.getName();
        return this.userRepository.findByUserName(userId)
                .orElseThrow(
                        () -> new UserNotFoundException(userId));
    }

}