package lt.akademija.it.disease;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Diseases")
public class Disease implements Serializable {

	@Id
	@GeneratedValue(generator = "optimized-sequence")
	private long id;

	private String description;

	@Column
	private String code;

	private int count; // indicates a number of times disease has been a cause of an appointment

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
