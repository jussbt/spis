package lt.akademija.it.disease;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/diseases")
public class DiseaseController {

    @Autowired
    DiseaseService diseaseService;

    @GetMapping()
    @PreAuthorize("hasAnyRole('ROLE_DOCTOR')")
    @ResponseStatus(HttpStatus.OK)
    public List<Disease> specializationList() {
        return diseaseService.getAll();
    }

    @GetMapping("/{parameter}")
    @PreAuthorize("hasAnyRole('ROLE_DOCTOR')")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<Disease> specializationListByParameter(@PathVariable String parameter) {
        return diseaseService.getAllByParameter(parameter);
    }

    @GetMapping("/statistics")
    public DiseaseStatistics getDiseaseStatistics() {
        return diseaseService.getDiseaseStatistics();
    }

}