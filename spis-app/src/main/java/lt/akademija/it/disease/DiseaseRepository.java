package lt.akademija.it.disease;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DiseaseRepository extends JpaRepository<Disease, Long> {
    Disease findById(Long id);

    List<Disease> findAllByCodeContainingOrDescriptionContaining(String code, String Description);

    List<Disease> findTop10ByOrderByCountDesc();

    @Query("Select sum(d.count) from Disease d")
    int getTotalAppoinments();
}