package lt.akademija.it.disease;

import java.util.ArrayList;
import java.util.List;

public class DiseaseStatistics {

    private List<Disease> top10Diseases = new ArrayList<>();
    private int totalAppoinments;

    public List<Disease> getTop10Diseases() {
        return top10Diseases;
    }

    public void setTop10Diseases(List<Disease> top10Diseases) {
        this.top10Diseases = top10Diseases;
    }

    public int getTotalAppoinments() {
        return totalAppoinments;
    }

    public void setTotalAppoinments(int totalAppoinments) {
        this.totalAppoinments = totalAppoinments;
    }
}
