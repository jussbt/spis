package lt.akademija.it.disease;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class DiseaseService {
    @Autowired
    DiseaseRepository diseaseRepository;

    @Transactional
    public List<Disease> getAll() {
        return diseaseRepository.findAll();
    }

    @Transactional
    public List<Disease> getAllByParameter(@PathVariable String parameter) {
        return diseaseRepository.findAllByCodeContainingOrDescriptionContaining(parameter, parameter);
    }

    @Transactional
    public DiseaseStatistics getDiseaseStatistics() {
        DiseaseStatistics diseaseStatistics = new DiseaseStatistics();
        diseaseStatistics.setTop10Diseases(diseaseRepository.findTop10ByOrderByCountDesc());
        diseaseStatistics.setTotalAppoinments(diseaseRepository.getTotalAppoinments());
        return diseaseStatistics;
    }
}