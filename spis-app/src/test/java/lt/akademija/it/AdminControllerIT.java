//package lt.akademija.it;
//
//import lt.akademija.it.specialization.Specialization;
//import lt.akademija.it.specialization.SpecializationDTO;
//import lt.akademija.it.user.Doctor;
//import lt.akademija.it.user.DoctorDTO;
//import lt.akademija.it.user.Patient;
//import lt.akademija.it.user.PatientDTO;
//import org.hamcrest.CoreMatchers;
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.web.bind.annotation.ResponseStatus;
//
//import javax.print.Doc;
//import javax.xml.ws.Response;
//import java.net.URI;
//import java.util.Date;
//import java.util.List;
//
//import static org.hamcrest.CoreMatchers.equalTo;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {SpisAppApplication.class})
//public class AdminControllerIT {
//
//    @Autowired
//    TestRestTemplate rest;
//
//    @Test
//    //this tests checks if we can get List of specializations , add specialization , remove specialization and we cannot add specialization with same names;
//    public void tryToCreateSpecialization() {
//        String specializationName = "testavimas";
//
//        //Checks if We can get lIST and response
//        ResponseEntity<List> response = rest.getForEntity("/api/specialization", List.class);
//        Assert.assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
//
//        //Size of all specializations
//        final SpecializationDTO specialization = new SpecializationDTO();
//        specialization.setName(specializationName);
//        rest.postForLocation("/api/specialization/add", specialization);
//        //Size of all specializations
//        List<Specialization> listOfSpecialization = rest.getForEntity("/api/specialization", List.class).getBody();
//        Assert.assertThat(listOfSpecialization.size() - response.getBody().size(), CoreMatchers.is(1));
//        ResponseEntity tryToCreateWithSameName = rest.postForEntity("/api/specialization/add", specialization, Void.class);
//        Assert.assertThat(tryToCreateWithSameName.getStatusCodeValue(), equalTo(500));
//        rest.delete("/api/specialization/delete/" + specializationName);
//        listOfSpecialization = rest.getForEntity("/api/specialization", List.class).getBody();
//        Assert.assertThat(listOfSpecialization.equals(response.getBody()), CoreMatchers.is(true));
//    }
//
//    @Test
//    //Create doctor , try to create with same name ,try to find him  and delete;
//    public void tryToCreateDoctor() {
//        DoctorDTO newDoctor = new DoctorDTO();
//        newDoctor.setFirstName("Testas");
//        newDoctor.setLastName("Testas");
//        newDoctor.setSpecialization("Testas");
//        newDoctor.setUserName("Testas1");
//        newDoctor.setPassword("Testas");
//        rest.delete("/api/admin/delete/" + newDoctor.getUserName());
//        ResponseEntity<Void> tryToCreateDoctor = rest.postForEntity("/api/admin/new/doctor", newDoctor, Void.class);
//        Assert.assertThat(tryToCreateDoctor.getStatusCode(), equalTo(HttpStatus.CREATED));
//        //try to create second time;
//        tryToCreateDoctor = rest.postForEntity("/api/admin/new/doctor", newDoctor, Void.class);
//        Assert.assertThat(tryToCreateDoctor.getStatusCode(), equalTo(HttpStatus.CONFLICT));
//
//        //Try to find him:
//        ResponseEntity<List> findDoctor = rest.postForEntity("/api/admin/finddoctor", newDoctor, List.class);
//        Assert.assertThat(findDoctor.getBody().size(), equalTo(1));
//        rest.delete("/api/admin/delete/" + newDoctor.getUserName());
//    }
//
//    //Create Patient and assign him to a doctor
//    @Test
//    public void trytoCreatePatient() {
//        final String userName = "Testas";
//        rest.delete("/api/admin/delete/" + userName);
//        PatientDTO patient = new PatientDTO();
//        patient.setFirstName("Testas");
//        patient.setLastName("Testas");
//        patient.setPassword("Testas");
//        patient.setBirthDate(new Date());
//        patient.setPersonalCode("39999999099");
//        patient.setUserName(userName);
//
//
//        ResponseEntity<Void> tryToCreatePatient = rest.postForEntity("/api/admin/new/patient", patient, Void.class);
//        Assert.assertThat(tryToCreatePatient.getStatusCode(), equalTo(HttpStatus.CREATED));
//        //Same information - Error.First - same personal ID.
//        tryToCreatePatient = rest.postForEntity("/api/admin/new/patient", patient, Void.class);
//        Assert.assertThat(tryToCreatePatient.getStatusCode(), equalTo(HttpStatus.FOUND));
//
//        patient.setPersonalCode("49999999999");
//        tryToCreatePatient = rest.postForEntity("/api/admin/new/patient", patient, Void.class);
//        Assert.assertThat(tryToCreatePatient.getStatusCode(), equalTo(HttpStatus.CONFLICT));
//        patient.setPersonalCode("39999999099");
//        ResponseEntity<List> findPatient = rest.getForEntity("/api/admin/findpatients/" + patient.getPersonalCode(), List.class);
//        Assert.assertThat(findPatient.getBody().size(), equalTo(1));
//        rest.delete("/api/admin/delete/" + userName);
//
//
//    }
//}
