//package lt.akademija.it;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.json.JacksonTester;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.ResultActions;
//import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//import lt.akademija.it.user.Admin;
//import lt.akademija.it.user.Doctor;
//import lt.akademija.it.user.User;
//import lt.akademija.it.user.UserController;
//import lt.akademija.it.user.UserRepository;
//
//@RunWith(SpringRunner.class)
//public class UserMvcTester {
//
//	private MockMvc mockMvc;
//	@Autowired
//	private TestRestTemplate restTemplate;
//
//	@MockBean
//	private UserRepository userRepository;
//
//	private JacksonTester<Doctor> json;
//
//	@InjectMocks
//	private UserController controller;
//
//	@Autowired
//	private WebApplicationContext wac;
//
//	@Before
//	public void setup() {
//		MockitoAnnotations.initMocks(this);
//		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
//	}
//	@Autowired
//	ObjectMapper objectMapper;
//
//	@Test
//	public void shouldReturOKStatus() throws Exception {
//		// when
//		ResponseEntity<User> response = restTemplate.getForEntity("/api/admin/finduserById/2", User.class);
//
//		// then
//		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
//	}
//
//	@Test
//	public void canCreateANewDoctor() throws Exception {
//
//		 Admin user = new Admin("name", "lastname","username","password");
//		 ((ResultActions) ( (MockHttpServletRequestBuilder) mockMvc.perform( post("/api/admin/new/admin")))
//		 .contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(user)))
//		 .andExpect(status().isCreated());
//
//	}
//}
