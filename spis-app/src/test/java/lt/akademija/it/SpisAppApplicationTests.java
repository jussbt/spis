package lt.akademija.it;

import lt.akademija.it.user.UserController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class SpisAppApplicationTests  {

	@Autowired
	private TestRestTemplate rest;

	@Autowired
	private UserController controller;


	@LocalServerPort
	private int port;

	@Test
	public void exampleTest() {
		String body = this.rest.getForObject("/", String.class);
		assertThat(body).contains("DOCTYPE");
	}

	@Test
	public void testExample() {
		assertThat(controller).isNotNull();

	}

	@Test
	public void checkAddress() throws Exception {
		assertThat(this.rest.getForObject("http://localhost:" + port + "/", String.class)).contains("DOCTYPE");
	}

}
