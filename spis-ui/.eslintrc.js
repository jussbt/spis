module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true
    }, 
    "parser": "babel-eslint",
    "extends": ["eslint:recommended", "plugin:react/recommended"],
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true
        },
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "indent": [
            "error",
            4
        ],
        "linebreak-style": 
            ["error", 
            (require("os").EOL === "\r\n" ? "windows" : "unix")],
        "semi": [
            "error",
            "always"
        ],
        "react/prop-types": 0,
        "no-unused-vars": ["error", { "vars": "all", "args": "after-used", "ignoreRestSiblings": false }]
    }      
};