import React , { Component } from 'react';
import { Dropdown } from 'primereact/components/dropdown/Dropdown';
import { CreateVaistininkas } from './CreateVaistininkas';
import { AdminNavigation } from './AdminNavigation';
import { CreateDoctor } from './CreateDoctor';
import { CreatePatient } from './CreatePatient';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';

export class AdminCreatesWindow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            roles: '',
            specList:[]
        };
    }

    componentDidMount() {
        axios.get('http://localhost:8080/api/specialization')
            .then((response) => {
                const pasirinkimai = response.data.map(specializacija => {
                    return { label: specializacija.name, value: specializacija.name };
                });
                this.setState({
                    specList: pasirinkimai
                });
            });
    }   

    onRolesChange = (e) => {
        this.setState({ roles: e.value });
    }

    rolesTemplate(option) {
        if (!option.value) {
            return option.label;
        } else {
            var logoPath = 'http://www.guibingzhuche.com/data/out/319/1897574.png';
            return (
                <div className="ui-helper-clearfix">
                    <img alt={option.label}
                        src={logoPath} style={{ display: 'inline-block', margin: '5px 0 0 5px' }} width="24" />
                    <span style={{ float: 'right', margin: '.5em .25em 0 0' }}>{option.label}</span>
                </div>
            );
        }
    }
    render() {
        var roles = [
            { label: 'Pacientas', value: 'patient' },
            { label: 'Gydytojas', value: 'doctor' },
            { label: 'Vaistininkas', value: 'pharmacist' }
        ];

        return (
            <div>
                <AdminNavigation history={this.props.history} />
                <form className="form-horizontal" style={{margin: 'auto', marginTop: 20, width:'50%'}}>
                    <div className="form-group">
                        <label className="col-sm-3 control-label">Vartotojo tipas</label>
                        <div className="col-sm-7">
                            <Dropdown
                                value={this.state.roles}
                                options={roles}
                                onChange={this.onRolesChange}
                                autoWidth={false}
                                itemTemplate={this.rolesTemplate}
                                style={{ width: '100%' }}
                                placeholder=""
                            />
                        </div>
                    </div>
                </form>

                {this.state.roles === 'pharmacist' ?
                    <CreateVaistininkas />
                    :
                    <div>
                    </div>
                }
                {this.state.roles === 'doctor' ?
                    <CreateDoctor specOptions={this.state.specList}/>
                    :
                    <div>
                    </div>
                }
                {this.state.roles === 'patient' ?
                    <CreatePatient />
                    :
                    <div>
                    </div>  
                }
            </div>
        );
    }
}