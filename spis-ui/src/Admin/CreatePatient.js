import React from 'react';
import axios from 'axios';
import classNames from 'classnames';
import validator from 'validator';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button } from 'primereact/components/button/Button';
import { Calendar } from 'primereact/components/calendar/Calendar';
import { InputText } from 'primereact/components/inputtext/InputText';
import { Messages } from 'primereact/components/messages/Messages';

export class CreatePatient extends React.Component {

    constructor() {
        super();
        this.state = {
            birthDate: { value: null, isValid: true, message: '' },
            firstname: { value: '', isValid: true, message: '' },
            lastname: { value: '', isValid: true, message: '' },
            username: { value: '', isValid: true, message: 'Prisijungimo varde gali būti tik raidės ir skaitmenys' },
            personId: { value: '', isValid: true, message: '' },
            today: { value: new Date() }
        };
    }

    handleChange = (e) => {
        var state = this.state;
        state[e.target.name].value = e.target.value;
        this.setState(state);
    }

    handleBirthdateChange = (e) => {
        var state = this.state;
        state['birthDate'].value = e.value;
        this.setState(state);
    }

    // funkcija tikrinanti, ar input sudaro vien raidės
    isAlpha = (inputText) => {
        const alphaExp = /^[a-žA-Ž]+$/;
        return inputText.match(alphaExp) ? true : false;
    }

    isNumber = (inputText) => {
        const numbExp = /^[0-9]+$/;
        return inputText.match(numbExp) ? true : false;
    }

    isPersonIdValid = () => {
        const { birthDate, personId } = this.state;
        var month = 0;
        var day = 0;
        var year = birthDate.value.getFullYear().toString().substring(2, 4);
        if (birthDate.value.getMonth() < 9) {
            month = 0 + (birthDate.value.getMonth() + 1).toString();
        } else {
            month = (birthDate.value.getMonth() + 1).toString();
        }
        if (birthDate.value.getDate() < 10) {
            day = 0 + (birthDate.value.getDate()).toString();
        } else {
            day = (birthDate.value.getDate()).toString();
        }
        var personalIdShouldBe = year + month + day;
        var personalIdIs = personId.value.toString().substring(1, 7);

        return personalIdShouldBe !== personalIdIs;
    }
  
    formIsValid = () => {
        var state = this.state;
        // eslint-disable-next-line
        if (!(this.state.personId.value.substring(0,1)==3||this.state.personId.value.substring(0,1)==4 ||
        // eslint-disable-next-line
        this.state.personId.value.substring(0,1)==5||this.state.personId.value.substring(0,1)==6 )){
            state.personId.isValid = false;
            state.personId.message = "Asmens kodas prasideda 3,4,5,6";
            this.setState(state);
            return false;
        }
        if (!this.isAlpha(state.firstname.value)) {
            state.firstname.isValid = false; //this will trigger the has-error class
            state.firstname.message = 'Neteisingai įvestas vardas'; //will be displayed in the help-block for the email input
            this.setState(state);
            return false;
        }

        if (!this.isAlpha(state.lastname.value)) {
            state.lastname.isValid = false;
            state.lastname.message = 'Neteisingai įvesta pavardė';
            this.setState(state);
            return false;
        }
        
        if (!validator.isAlphanumeric(state.username.value)) {
            state.username.isValid = false;
            state.username.message = "Neteisingai įvestas prisijungimo vardas";
            this.setState(state);
            return false;
        }

        if (state.birthDate.value == null) {
            state.birthDate.isValid = false;
            state.birthDate.message = "Nepasirinkta gimimo data";
            this.setState(state);
            return false;
        }
        if (isNaN(state.personId.value) === true) {
            state.personId.isValid = false;
            state.personId.message = "Asmens kodą sudaro tik skaičiai";
            this.setState(state);
            return false;
        }


        if (validator.isEmpty(state.personId.value)) {
            state.personId.isValid = false;
            state.personId.message = "Neįvestas asmens kodas";
            this.setState(state);
            return false;
        }

        if (this.isPersonIdValid()) {
            state.personId.isValid = false;
            state.personId.message = "Nesutampa asmens kodas su data";
            this.setState(state);
            return false;
        }

        return true;
    }

    resetValidationStates = () => {
        var state = this.state;
        // eslint-disable-next-line
        Object.keys(state).map(key => {
            if (key === 'username') {
                state[key].isValid = true;
                state[key].message = 'Prisijungimo varde gali būti tik raidės ir skaitmenys';
            } else if (state[key].hasOwnProperty('isValid')) {
                state[key].isValid = true;
                state[key].message = '';
            }
        });
        this.setState(state);
    }
    register = (e) => {
        e.preventDefault();
        this.resetValidationStates();
        if (this.formIsValid()) {
            const { firstname, lastname, username, birthDate, personId } = this.state;
            const patient = {
                firstName: firstname.value,
                lastName: lastname.value,
                userName: username.value,
                birthDate: birthDate.value,
                personalCode: personId.value,
                password: username.value
            };
            axios.post("http://localhost:8080/api/admin/new/patient", patient)
                .then(response => {
                    if (response.status === 201) {
                        this.messages.show({ severity: 'success', summary: 'Vartotojas sėkmingai sukurtas', life: 4000 });
                        this.cancel(e);
                    }
                })
                .catch(error => {
                    if (error.response.status === 409) {
                        this.messages.show({ severity: 'error', summary: 'Vartotojas tokiu prisijungimo vardu jau yra', life: 4000 });
                    }
                    if (error.response.status === 302) {
                        this.messages.show({ severity: 'error', summary: 'Vartotojas tokiu asmens kodu jau yra', life: 4000 });
                    }

                });
        }
    }

    cancel = (e) => {
        e.preventDefault();
        this.resetValidationStates();
        var state = this.state;
        // eslint-disable-next-line
        Object.keys(state).filter(key => key !== 'today').map(key => {
            if (key === 'birthDate') {
                state[key].value = null;
            } else {
                state[key].value = '';
            }
        });
        this.setState(state);
    }

    render() {
        let lt = {
            firstDayOfWeek: 1,
            dayNames: ["Sekmadienis","Pirmadienis", "Antradienis", "Trečiadienis", "Ketvirtadienis", "Penktadienis", "Šeštadienis"],
            dayNamesShort: ["sek","pirm", "antr", "treč", "ketv", "penk", "šeš"],
            dayNamesMin: ["S","P", "A", "T", "K", "P", "Š"],
            monthNames: ["Sausis", "Vasaris", "Kovas", "Balandis", "Gegužė", "Birželis", "Liepa", "Rugpjūtis", "Rugsėjis", "Spalis", "Lapkritis", "Gruodis"],

        };
        const { firstname, lastname, username, personId, birthDate, today } = this.state;

        /*
        Each of the group classes below will include the 'form-group' class, and will only
        include the 'has-error' class if the isValid value is false.
        */
        const firstnameGroupClass = classNames('form-group', { 'has-error': !firstname.isValid });
        const lastnameGroupClass = classNames('form-group', { 'has-error': !lastname.isValid });
        const usernameGroupClass = classNames('form-group', { 'has-error': !username.isValid });
        const birthdateGroupClass = classNames('form-group', { 'has-error': !birthDate.isValid });
        const personIdGroupClass = classNames('form-group', { 'has-error': !personId.isValid });

        const dropdownSize = {
            width: '100%'
        };

        return (
            <form className="form-horizontal" onSubmit={this.register} style={{ margin: 'auto', marginTop: 20, width: '50%' }}>

                <div className={firstnameGroupClass}>
                    <label className="col-sm-3 control-label">Vardas</label>
                    <div className="col-sm-7">
                        <InputText
                            name="firstname"
                            placeholder="Įveskite vardą"
                            maxLength="20"
                            style={dropdownSize}
                            value={firstname.value}
                            onChange={this.handleChange}
                        />
                        <span className="help-block">{firstname.message}</span>
                    </div>
                </div>

                <div className={lastnameGroupClass}>
                    <label className="col-sm-3 control-label">Pavardė</label>
                    <div className="col-sm-7">
                        <InputText
                            name="lastname"
                            placeholder="Įveskite pavardę"
                            maxLength="20"
                            style={dropdownSize}
                            value={lastname.value}
                            onChange={this.handleChange}
                        />
                        <span className="help-block">{lastname.message}</span>
                    </div>
                </div>

                <div className={usernameGroupClass}>
                    <label className="col-sm-3 control-label">Prisijungimo vardas</label>
                    <div className="col-sm-7">
                        <InputText
                            name="username"
                            value={username.value}
                            placeholder="Įveskite prisijungimo vardą"
                            style={dropdownSize}
                            onChange={this.handleChange}
                            maxLength="20"
                        />
                        <span className="help-block">{username.message}</span>
                    </div>
                </div>

                <div className={birthdateGroupClass}>
                    <label className="col-sm-3 control-label">Gimimo data</label>
                    <div className="col-sm-7">
                        <Calendar
                            locale={lt}
                            placeholder="Pasirinkite datą"
                            dateFormat="yy-mm-dd"
                            style={{ display: 'block' }}
                            inputStyle={dropdownSize}
                            maxDate={today.value}
                            yearRange="1918:2018"
                            monthNavigator="true"
                            yearNavigator="true"
                            readOnlyInput={true}
                            value={birthDate.value}
                            onChange={this.handleBirthdateChange}
                        />
                        <span className="help-block">{birthDate.message}</span>
                    </div>
                </div>

                <div className={personIdGroupClass}>
                    <label className="col-sm-3 control-label">Asmens kodas</label>
                    <div className="col-sm-7">
                        <InputText
                            minLength={11}
                            maxLength={11}
                            keyfilter="int"
                            placeholder="Įveskite asmens kodą"
                            style={dropdownSize}
                            name="personId"
                            value={personId.value}
                            onChange={this.handleChange}
                        />
                        <span className="help-block">{personId.message}</span>
                    </div>
                </div>

                <div className="form-group">
                    <div className="col-sm-offset-3 col-sm-7">
                        <Button label="Registruoti"  className="ui-button-success" style={{width: '48%'}}/>
                        <Button label="Atšaukti" onClick={this.cancel} className="ui-button-danger" style={{width: '48%'}}/><br />                           
                    </div>
                </div>
                <Messages ref={(el) => { this.messages = el; }} style={{ width: 350, marginLeft: 115 }}></Messages>
            </form >
        );
    }
}    