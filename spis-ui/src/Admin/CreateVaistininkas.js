import React from 'react';
import axios from 'axios';
import classNames from 'classnames';
import validator from 'validator';
import { Dropdown } from 'primereact/components/dropdown/Dropdown';
import { Button } from 'primereact/components/button/Button';
import { Messages } from 'primereact/components/messages/Messages';
import { InputText } from 'primereact/components/inputtext/InputText';

export class CreateVaistininkas extends React.Component {

    constructor() {
        super();
        this.state = {
            firstname: {value: '', isValid: true, message: ''},
            lastname: {value: '', isValid: true, message: ''},
            username: {value: '', isValid: true, message: 'Prisijungimo varde gali būti tik raidės ir skaitmenys'},
            enterpriseType: {value: 'UAB'},
            enterpriseName: {value: '', isValid: true, message: ''}
        };
    }

    handleChange = (e) => {
        var state = this.state;
        state[e.target.name].value = e.target.value;
        this.setState(state);
    }

    handleEnterpriseTypeChange = (e) => {
        var state = this.state;
        state['enterpriseType'].value = e.value;
        this.setState(state);
    }

    // funkcija tikrinanti, ar input sudaro vien raidės
    isAlpha = (inputText) => {
        const alphaExp = /^[a-žA-Ž]+$/;
        return inputText.match(alphaExp) ? true : false;   
    }

    formIsValid = () => {
        var state = this.state;
  
        if (!this.isAlpha(state.firstname.value)) {
            state.firstname.isValid = false; //this will trigger the has-error class
            state.firstname.message = 'Neteisingai įvestas vardas'; //will be displayed in the help-block for the email input
            this.setState(state);
            return false;
        }
    
        if (!this.isAlpha(state.lastname.value)) {
            state.lastname.isValid = false;
            state.lastname.message = 'Neteisingai įvesta pavardė';
            this.setState(state);
            return false;
        }
  
        if (!validator.isAlphanumeric(state.username.value)) {
            state.username.isValid = false;
            state.username.message = "Neteisingai įvestas prisijungimo vardas";
            this.setState(state);
            return false;
        }

        // input can only contain alphanumeric characters including Lithuanian characters and spaces
        if (!/^([a-žA-Ž0-9 ]+)$/.test(state.enterpriseName.value)) {
            state.enterpriseName.isValid = false;
            state.enterpriseName.message = "Neteisingai įvestas įmonės pavadinimas";
            this.setState(state);
            return false;
        }
        
        return true;
    }

    resetValidationStates = () => {
        var state = this.state;
        // eslint-disable-next-line
        Object.keys(state).map(key => {
            if (key === 'username') {
                state[key].isValid = true;
                state[key].message = 'Prisijungimo varde gali būti tik angliškos raidės ir skaitmenys';
            } else if (state[key].hasOwnProperty('isValid')) {
                state[key].isValid = true;
                state[key].message = '';
            }
        });
        this.setState(state);
    }

    register = (e) => {
        this.resetValidationStates();
        if (this.formIsValid()) {
            const {firstname, lastname, username, enterpriseName, enterpriseType} = this.state;
            const pharmacist = {
                firstName: firstname.value,
                lastName: lastname.value,
                userName: username.value,
                password: username.value,
                workplace: enterpriseType.value + ' ' + enterpriseName.value
            };

            axios.post("http://localhost:8080/api/admin/new/pharmacist", pharmacist)
                .then((response) => {
                    if (response.status === 201) {
                        this.messages.show({ severity: 'success', summary: 'Vartotojas sėkmingai sukurtas', life: 5000 });
                        this.cancel(e);
                    }
                })
                .catch(error => {
                    if (error.response.status === 409) {
                        this.messages.show({ severity: 'error', summary: 'Vartotojas tokiu prisijungimo vardu jau yra', life: 5000 });
                    }
                });
        }
        e.preventDefault();
    }

    cancel = (e) => {
        e.preventDefault();
        this.resetValidationStates();
        var state = this.state;
        // eslint-disable-next-line
        Object.keys(state).filter(key => key !== 'enterpriseType').map(key => {
            state[key].value = '';
        });
        this.setState(state);
    }

    render() {      
       
        var enterpriseTypes = [
            { label: 'VšĮ', value: 'VšĮ' },
            { label: 'UAB', value: 'UAB' },
            { label: 'AB', value: 'AB' },
            { label: 'MB', value: 'MB' }
        ];

        const {firstname, lastname, username, enterpriseName, enterpriseType} = this.state;

        /*
        Each of the group classes below will include the 'form-group' class, and will only
        include the 'has-error' class if the isValid value is false.
        */
        const firstnameGroupClass = classNames('form-group', {'has-error': !firstname.isValid});
        const lastnameGroupClass = classNames('form-group', {'has-error': !lastname.isValid});
        const usernameGroupClass = classNames('form-group', {'has-error': !username.isValid});
        const enterpriseGroupClass = classNames('form-group', {'has-error': !enterpriseName.isValid});

        const dropdownSize = {
            width: '100%'
        };

        return (
            <form className="form-horizontal" onSubmit={this.register} style={{margin: 'auto', marginTop: 20, width:'50%'}}>

                <div className={firstnameGroupClass}>
                    <label className="col-sm-3 control-label">Vardas</label>
                    <div className="col-sm-7">
                        <InputText
                            name="firstname"
                            style={dropdownSize}
                            value={firstname.value}
                            onChange={this.handleChange}
                            placeholder="Įveskite vardą"
                            maxLength="20"
                        />
                        <span className="help-block">{firstname.message}</span>
                    </div>
                </div>

                <div className={lastnameGroupClass}>
                    <label className="col-sm-3 control-label">Pavardė</label>
                    <div className="col-sm-7">
                        <InputText
                            value={lastname.value}
                            style={dropdownSize}
                            onChange={this.handleChange}
                            placeholder="Įveskite pavardę"
                            name="lastname"
                            maxLength="20"
                        />
                        <span className="help-block">{lastname.message}</span>
                    </div>
                </div>

                <div className={usernameGroupClass}>
                    <label className="col-sm-3 control-label">Prisijungimo vardas</label>
                    <div className="col-sm-7">
                        <InputText
                            value={username.value}
                            style={dropdownSize}
                            onChange={this.handleChange}
                            name="username"
                            maxLength="20"
                            placeholder="Įveskite prisijungimo vardą"
                        />
                        <span className="help-block">{username.message}</span>
                    </div>
                </div>

                <div className={enterpriseGroupClass}>
                    <label className="col-sm-3 control-label">Įmonė</label>
                    <div className="col-sm-7">
                        <Dropdown
                            value={enterpriseType.value}
                            options={enterpriseTypes}
                            placeholder="UAB"
                            name="enterpriseType"
                            onChange={this.handleEnterpriseTypeChange}
                            maxLength="20"
                        />
                        <InputText
                            value={enterpriseName.value}
                            style={dropdownSize}
                            onChange={this.handleChange}
                            placeholder="Įveskite įmonės pavadinimą"
                            name="enterpriseName"
                            maxLength="40"
                        />
                        <span className="help-block">{enterpriseName.message}</span>
                    </div>
                </div>

                <div className="form-group">
                    <div className="col-sm-offset-3 col-sm-7">
                        <Button label="Registruoti" className="ui-button-success" style={{width: '48%'}}/>
                        <Button label="Atšaukti" onClick={this.cancel} className="ui-button-danger" style={{width: '48%'}}/><br />                        
                    </div>
                </div>
                <Messages ref={(el) => { this.messages = el; }} style={{width: 350, marginLeft: 115}}></Messages>
            </form>
        );
    }
}
