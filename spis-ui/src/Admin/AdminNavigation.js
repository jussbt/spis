import React from 'react';
import { NavLink } from 'react-router-dom';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import logo from '../logo.png';
import { Toolbar } from 'primereact/components/toolbar/Toolbar';
import { Button } from 'primereact/components/button/Button';
import LogOutComponent from '../LoginLogout/LogOutComponent';
import PropTypes from 'prop-types';

export var AdminNavigation = (props) => {

    return (
        <Toolbar>
            <div className="ui-toolbar-group-left">
                <img src={logo} alt='' height="30px" width='auto'/>
                <NavLink to="/admin/new">
                    <Button label="Pridėti vartotoją" icon="fa-plus" />
                </NavLink>
                <NavLink to ="/admin/doctors">
                    <Button label="Gydytojų sąrašas" icon="fa-folder-open" className="ui-button-success" />
                </NavLink>               
            </div>
            <div className="ui-toolbar-group-right">
                <NavLink to="/changepassword">
                    <Button label="Slaptažodžio keitimas" icon="fa-check" className="ui-button-warning" />
                </NavLink>               
                <LogOutComponent history={props.history}/>
            </div>
        </Toolbar>
    );
};

LogOutComponent.propTypes = {
    history: PropTypes.object
};
