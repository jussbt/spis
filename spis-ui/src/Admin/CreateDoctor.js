import React from 'react';
import { Dropdown } from 'primereact/components/dropdown/Dropdown';
import { Button } from 'primereact/components/button/Button';
import { Messages } from 'primereact/components/messages/Messages';
import { InputText } from 'primereact/components/inputtext/InputText';
import axios from 'axios';
import classNames from 'classnames';
import validator from 'validator';

export class CreateDoctor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: {value: '', isValid: true, message: ''},
            lastname: {value: '', isValid: true, message: ''},
            username: {value: '', isValid: true, message: 'Prisijungimo varde gali būti tik raidės ir skaitmenys'},
            specialybe: {value: '', isValid: true, message: ''},
            speciality: {value: '', isValid: true, message: ''}, // selected doctor speciality from dropdown
            addSpeciality: {value: false}, // when true you can add more speciality components
            specialityOptions: {value: props.specOptions} , // dropdown list of specializations                            
        };
    }

    onSpecialityChange = (e) => {
        var state = this.state;
        state['speciality'].value = e.value;
        this.setState(state);        
    }

    addSpecialityFunction = (e) => {
        this.setState({ addSpeciality: {value: true }});
        e.preventDefault();
    }

    handleChange = (e) => {
        var state = this.state;
        state[e.target.name].value = e.target.value;
        this.setState(state);
    }

    // funkcija tikrinanti, ar input sudaro vien raidės
    isAlpha = (inputText) => {
        const alphaExp = /^[a-žA-Ž]+$/;
        return inputText.match(alphaExp) ? true : false;   
    }

    formIsValid = () => {
        var state = this.state;
  
        if (!this.isAlpha(state.firstname.value)) {
            state.firstname.isValid = false; //this will trigger the has-error class
            state.firstname.message = 'Neteisingai įvestas vardas'; //will be displayed in the help-block for the email input
            this.setState(state);
            return false;
        }
  
        if (!this.isAlpha(state.lastname.value)) {
            state.lastname.isValid = false;
            state.lastname.message = 'Neteisingai įvesta pavardė';
            this.setState(state);
            return false;
        }

        if (!validator.isAlphanumeric(state.username.value)) {
            state.username.isValid = false;
            state.username.message = "Neteisingai įvestas prisijungimo vardas";
            this.setState(state);
            return false;
        }

        if (validator.isEmpty(state.speciality.value)) {
            state.speciality.isValid = false;
            state.speciality.message = "Nepasirinkta specializacija";
            this.setState(state);
            return false;
        }

        if (!this.isAlpha(state.specialybe.value) && state.addSpeciality.value && validator.isEmpty(state.speciality.value)) {
            state.specialybe.isValid = false;
            state.specialybe.message = "Neteisingai įvesta nauja specializacija";
            this.setState(state);
            return false;
        }

        return true;
    }

    resetValidationStates = () => {
        var state = this.state;
        // eslint-disable-next-line
        Object.keys(state).map(key => {
            if (key === 'username') {
                state[key].isValid = true;
                state[key].message = 'Prisijungimo varde gali būti tik raidės ir skaitmenys';
            } else if (state[key].hasOwnProperty('isValid')) {
                state[key].isValid = true;
                state[key].message = '';
            }
        });
        this.setState(state);
    }

    register = (event) => {
        event.preventDefault();
        this.resetValidationStates(); //reset states before the validation procedure is run.
        if (this.formIsValid()) {
            const {firstname, lastname, username, speciality} = this.state;
            const doctor = {
                firstName: firstname.value,
                lastName: lastname.value,
                userName: username.value,
                specialization: speciality.value,
                password: username.value
            };
            
            axios.post("http://localhost:8080/api/admin/new/doctor", doctor)
                .then(response => {
                    if (response.status === 201) {                      
                        this.messages.show({ severity: 'success', summary: 'Vartotojas sėkmingai sukurtas', life: 4000 });
                        this.cancel(event);
                    }
                })
                .catch(error => {
                    if (error.response.status === 409) {
                        this.messages.show({ severity: 'error', summary: 'Vartotojas tokiu prisijungimo vardu jau yra', life: 4000 });
                    }
                });
        }
    }

    cancel = (event) => {
        event.preventDefault();
        this.resetValidationStates();
        var state = this.state;
        // eslint-disable-next-line
        Object.keys(state).filter(key => key !== 'specialityOptions').map(key => {
            if (key === 'addSpeciality') {
                state[key].value = false;
            } else {
                state[key].value = '';
            }
        });
        this.setState(state);
    }

    // Adds speciality to list
    addValue = (event) => {
        const {specialybe} = this.state;
        let spec = [{ label: specialybe.value, value: specialybe.value }];
        let specialybes = this.state.specialityOptions.value.concat(spec);
        const specialization = {
            name: specialybe.value
        };
        if (specialization.name.length>0){
            axios.post("http://localhost:8080/api/specialization/add/", specialization)
                .then(() => {
                    var state = this.state;
                    state["specialityOptions"].value = specialybes;
                    state["speciality"].value = specialybe.value;
                    state["specialybe"].value = '';
                    this.setState(state);
                });
        }
        event.preventDefault();
    }
   
    render() {
        const {firstname, lastname, username, specialybe, speciality, specialityOptions} = this.state;
        
        /*  Each of the group classes below will include the 'form-group' class, and will only
            include the 'has-error' class if the isValid value is false. */
        const firstnameGroupClass = classNames('form-group', {'has-error': !firstname.isValid});
        const lastnameGroupClass = classNames('form-group', {'has-error': !lastname.isValid});
        const usernameGroupClass = classNames('form-group', {'has-error': !username.isValid});
        const specialybeGroupClass = classNames('form-group', {'has-error': !specialybe.isValid});
        const specialityGroupClass = classNames('form-group', {'has-error': !speciality.isValid});

        const dropdownSize = {
            width: '100%'
        };
       
        return (
            <form className="form-horizontal" onSubmit={this.register} style={{margin: 'auto', marginTop: 20, width:'50%'}}>
            
                <div className={firstnameGroupClass}>
                    <label className="col-sm-3 control-label">Vardas</label>
                    <div className="col-sm-7">
                        <InputText
                            name="firstname"
                            value={firstname.value}
                            style={dropdownSize}
                            onChange={this.handleChange}
                            placeholder="Įveskite vardą"
                            maxLength="20"
                        />
                        <span className="help-block">{firstname.message}</span>
                    </div>
                </div>

                <div className={lastnameGroupClass}>
                    <label className="col-sm-3 control-label">Pavardė</label>
                    <div className="col-sm-7">
                        <InputText
                            name="lastname"
                            style={dropdownSize}
                            value={lastname.value}
                            onChange={this.handleChange}
                            maxLength="20"
                            placeholder="Įveskite pavardę"
                        />
                        <span className="help-block">{lastname.message}</span>
                    </div>
                </div>

                <div className={usernameGroupClass}>
                    <label className="col-sm-3 control-label">Prisijungimo vardas</label>
                    <div className="col-sm-7">
                        <InputText
                            name="username"
                            style={dropdownSize}
                            maxLength="20"
                            value={username.value}
                            onChange={this.handleChange}
                            placeholder="Įveskite prisijungimo vardą"
                        />
                        <span className="help-block">{username.message}</span>
                    </div>
                </div>

                <div className={specialityGroupClass}>
                    <label className="col-sm-3 control-label">Specializacija</label>
                    <div className="col-sm-7">
                        <Dropdown 
                            style={dropdownSize}
                            value={speciality.value}
                            options={specialityOptions.value}
                            name="speciality"
                            onChange={this.onSpecialityChange}
                            autoWidth={false}
                            size={10}
                            filter={true}
                            filterPlaceholder="Ieškoti"
                            filterBy="label,value"
                        />
                    </div>
                    <div className="col-sm-2" style={{paddingRight: '0', paddingLeft: '0'}}>
                        <Button label="Sukurti naują" onClick={this.addSpecialityFunction} className="ui-button-success" style={dropdownSize}/>
                    </div>
                    <div className="col-sm-offset-3 col-sm-10">
                        <span className="help-block">{speciality.message}</span>
                    </div>
                </div>

                {
                    this.state.addSpeciality.value ?

                        <div className={specialybeGroupClass}>
                            <div className="col-sm-offset-3 col-sm-7">
                                <InputText
                                    name="specialybe"
                                    style={dropdownSize}
                                    maxLength="30"
                                    value={specialybe.value}
                                    onChange={this.handleChange}
                                    placeholder="Įveskite specializaciją"
                                />
                            </div>
                            <div className="col-sm-2" style={{paddingRight: '0', paddingLeft: '0'}}>
                                <Button label="Pridėti" onClick={this.addValue} className="ui-button-success" style={dropdownSize}/>
                            </div>
                            <div className="col-sm-offset-2 col-sm-10">
                                <span className="help-block">{specialybe.message}</span>
                            </div>
                        </div>
                        :
                        <div>

                        </div>
                }

                <div className="form-group">
                    <div className="col-sm-offset-3 col-sm-7">           
                        <Button label="Registruoti" className="ui-button-success" style={{width: '48%'}}/>
                        <Button label="Atšaukti" onClick={this.cancel} className="ui-button-danger" style={{width: '48%'}}/><br />
                    </div>
                </div>
                
                <Messages ref={(el) => { this.messages = el; }} style={{width: 350, marginLeft: 115}}></Messages>
            </form >
        );
    }
}
