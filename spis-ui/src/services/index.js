import UserStorage from './userStorage';
import StatusService from './statusService';
 
//always export array, even if you have only one service
export default [UserStorage, StatusService];