import {Service} from 'react-services-injector';

class StatusService extends Service {
    constructor() {
        super();
        this.isAuthentificated = false;
    }
  
    // initialStatus() {
    //   this.isAuthentificated = false;
    // }
    toggle(){
        if (this.isAuthentificated ===false){
            this.isAuthentificated=true;
        }else {
            this.isAuthentificated =false;
        }
    }
    get statusIs() {
        return this.isAuthentificated;
    }
}
 
//"publicName" property is important if you use any kind of minimization on your JS
StatusService.publicName = 'StatusService';
 
export default StatusService;