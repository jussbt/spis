import {Service} from 'react-services-injector';

class UserStorage extends Service {
    constructor() {
        super();
        this.changeRole('none');
    }
  
    changeRole(userRole){
        localStorage.setItem('UserRole', userRole);
    }
    get currentRole() {
        return localStorage.getItem('UserRole');
    }
}
 
//"publicName" property is important if you use any kind of minimization on your JS
UserStorage.publicName = 'UserStorage';
 
export default UserStorage;