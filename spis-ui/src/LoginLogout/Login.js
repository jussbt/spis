import React from 'react';
import './login.css';
import logo from '../logo.png';
import 'primereact/resources/themes/omega/theme.css';
import 'primereact/resources/primereact.min.css';
import 'font-awesome/css/font-awesome.css';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import {injector} from 'react-services-injector';

export class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            accountName: '',
            password: '',
        };
    }

    handleClick = (e) => {
        e.preventDefault();
        let bodyFormData = new FormData(); // talpinam username ir password siuntimui
        bodyFormData.set('username', this.state.accountName);
        bodyFormData.set('password', this.state.password);

        axios.post('http://localhost:8080/login', bodyFormData, {      
            config: { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
        })
            .then(response => {
                if (response.status === 200) {
                    const {role} = response.data;
                    const {UserStorage} = injector.get();                  
                    if (role === 'ROLE_ADMIN'){
                        UserStorage.changeRole(btoa('kaspa'));
                        this.props.history.push("/admin");
                    }else if (role === 'ROLE_DOCTOR'){
                        UserStorage.changeRole(btoa('ignas'));
                        this.props.history.push("/doctor");
                    }else if (role === 'ROLE_PATIENT'){
                        UserStorage.changeRole(btoa('algis'));
                        this.props.history.push("/patient/appointments");
                    } else if (role === 'ROLE_PHARMACIST'){
                        UserStorage.changeRole(btoa('justas'));
                        this.props.history.push("/pharmacist/prescriptions");
                    } 
                }
          
            })
            .catch(() => {
                this.setState({
                    badLogin:"Blogi prisijungimo duomenys"
                });
            });
    }

    render() {
        const {UserStorage} = injector.get();  
          
        var badLoginStyle = {
            color: 'red',
            fontWeight: 'bold'    
        };
        if (UserStorage.currentRole != 'none') {
            const user = atob(UserStorage.currentRole);
            switch (user) {
            case 'kaspa':
                return <Redirect to='/admin' />;
            case 'ignas':
                return <Redirect to='/doctor' />;
            case 'algis':
                return <Redirect to='/patient/appointments' />;
            case 'justas':
                return <Redirect to='/pharmacist' />;
            }
        }
        return (
            <div className="login-box animated fadeInUp">
                <div className="box-header">
                    <button onClick={() => this.props.history.push("/")} style={{padding: 0}}>
                        <img src={logo} alt='' height="35px" width='auto'/>
                    </button>
                </div>
                <form style={{paddingTop: 15}}>
                    <label htmlFor="username">Prisijungimo vardas</label><br />
                    <input value={this.state.accountName} onChange={(e) => this.setState({ accountName: e.target.value })} /><br />
                    <label htmlFor="password">Slaptažodis</label><br />
            
                    <input type="password" value={this.state.password} onChange={(e) => this.setState({ password: e.target.value })} />
                    <br />
                    <label style={badLoginStyle}> {this.state.badLogin}</label>
                    <br/>
                    <button onClick={this.handleClick}> prisijungti </button>
                </form>
            </div>
        );          
    }
}

export default injector.connect(Login);