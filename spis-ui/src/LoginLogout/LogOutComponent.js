import React from 'react';
import axios from 'axios';
import {Button} from 'primereact/components/button/Button';
import PropTypes from 'prop-types';
import {injector} from 'react-services-injector';

var LogOutComponent = (props) => {
    var handleLogout = () => {
        axios.get('http://localhost:8080/logout')
            .then(response => {
                if (response.status === 200) {
                    const {UserStorage} = injector.get();
                    UserStorage.changeRole(null);
                    props.history.push("/");
                }
            })
            .catch(() => {
                
            });
    };

    return (
        <Button id="logout" onClick={handleLogout} className="ui-button-danger" label="Atsijungti" />
    );
};

LogOutComponent.propTypes = {
    history: PropTypes.object
};

export default LogOutComponent;