import React, { Component } from 'react';
import axios from '../../node_modules/axios';
import { InputText } from 'primereact/components/inputtext/InputText';
import { AllDoctorsComponent } from './AllDoctorsComponent';
import { NavLink } from 'react-router-dom';
import { Button } from 'primereact/components/button/Button';
import { Dropdown } from 'primereact/components/dropdown/Dropdown';
import { Messages } from 'primereact/components/messages/Messages';

export class AllDoctorsConteiner extends Component {
    constructor() {
        super();
        this.state = {
            filters: {},
            doctors: [],
            specList: [],
            specListValue:null,
            doctorLastName:null
        };
    }
    componentDidMount() {
        axios.get('http://localhost:8080/api/specialization')
            .then((response) => {
                const pasirinkimai = response.data.map(specializacija => {
                    return { label:specializacija.name, value:specializacija.name };
                });
                this.setState({
                    specList: pasirinkimai
                });
            });
    }


    actionTemplate = (rowData, column) => {
        return <div>
            <NavLink to={"/admin/patients/" + rowData.userId} >
                <Button label="Priskirti " icon="fa-plus" className="ui-button-secondary"/>
            </NavLink>
        </div>;
    }

    /**sending specialization , we need to remove last symbol. Why? I do not know , but it adds one 
     * additional space for no reason...
     */
    findDoctor=()=>{
        if(this.state.doctorLastName==null){
            this.messages.show({ severity: 'error', summary: 'Nepasirinkta gydytojo pavardė', life: 5000 }); 
            return;
        }
        if(this.state.specListValue==null){
            this.messages.show({ severity: 'error', summary: 'Nepasirinkta gydytojo specializacija', life: 5000 }); 
            return;
        }
        const firstnameUpper = this.state.doctorLastName.charAt(0).toUpperCase() + this.state.doctorLastName.slice(1);
        const doctor = {
            lastName: firstnameUpper,
            specialization: this.state.specListValue.substring(0, this.state.specListValue.length-1 )
        };

        axios.post("http://localhost:8080/api/admin/finddoctor",doctor)
            .then(response => {
                if (response.data.length==0){
                    this.messages.show({ severity: 'error', summary: 'Nerastas toks gydytojas', life: 5000 }); 
                }else{
                    this.setState({
                        doctors: response.data
                    });
                }
               
            })
            
            .catch(() => {
                
            });
           
    }
    onSpecialityChange=(e)=>{
        this.setState({
            specListValue:e.value
        });
    }

    render() {
        const dropdownSize = {
            width: '220px',
            height: '29px',
            margin: '0 10px 0 10px'
        }; 
        const bodyMessages =
        <div>
            <Messages ref={(el) => { this.messages = el; }} style={{ margin: '0 auto'}}></Messages>
        </div>;
        var header = <div style={{ 'textAlign': 'left' }}>
            <i className="fa fa-search" style={{ margin: '4px 4px 0 0' }}></i>
            <InputText 
                type="search"
                onInput={(e) => this.setState({ doctorLastName: e.target.value })} 
                placeholder="Įveskite daktaro pavardę" 
                size="20" 
            />
            <Dropdown   style={dropdownSize}  value={this.state.specListValue}  options={this.state.specList}
                name="speciality"
                onChange={this.onSpecialityChange}
                placeholder="Daktaro specializacija"
                filter={true}
                filterPlaceholder="Ieškoti"
                filterBy="label,value"
            />
            <Button label="Surasti" onClick={this.findDoctor}/>
        </div>;
        return (
            <div>
                <AllDoctorsComponent
                    history={this.props.history}
                    doctors={this.state.doctors}
                    header={header}
                    globalFilter={this.state.globalFilter}
                    items={this.items}
                    body={this.actionTemplate}
                    bodyMessage={bodyMessages}

                />
            </div>
        );
    }
}