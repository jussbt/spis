import React from 'react';
import { AdminNavigation } from '../Admin/AdminNavigation';
import { DataTable } from 'primereact/components/datatable/DataTable';
import { Column } from 'primereact/components/column/Column';

export var AllDoctorsComponent = (props) => {
    
    const textAlignStyle={
        textAlign:'center'
    };
    
    return (

        <div>

            <AdminNavigation history={props.history} />

            <div className="content-section implementation">
                <DataTable 
                    responsive={true}
                    value={props.doctors} 
                    header={props.header} 
                    globalFilter={props.globalFilter} 
                    emptyMessage="Įrašų nėra"
                >
                    <Column style={textAlignStyle} field="firstname" header="Vardas" />
                    <Column style={textAlignStyle} field="lastname" header="Pavardė" />                   
                    <Column style={textAlignStyle} field="specialization" header="Specializacija" />
                    <Column header="Pacientų priskirimas" body={props.body} style={{ textAlign: 'center', width: '9em' }} />
                </DataTable>

            </div>
            {props.bodyMessage}
        </div>
    );   
};