import React from 'react';
import { DataTable } from 'primereact/components/datatable/DataTable';
import { Column } from 'primereact/components/column/Column';
import { DoctorNavigationComponent } from '../DoctorNavigation/DoctorNavigationComponent';
import { Calendar } from 'primereact/components/calendar/Calendar';
import { Dropdown } from 'primereact/components/dropdown/Dropdown';
import {Accordion,AccordionTab} from 'primereact/components/accordion/Accordion';
export var ScheduleComponent = (props) => {

    const textAlignStyle = {
        textAlign: 'center'
    };
    let lt = {
        firstDayOfWeek: 1,
        dayNames: ["Sekmadienis","Pirmadienis", "Antradienis", "Trečiadienis", "Ketvirtadienis", "Penktadienis", "Šeštadienis"],
        dayNamesShort: ["sek","pirm", "antr", "treč", "ketv", "penk", "šeš"],
        dayNamesMin: ["S","P", "A", "T", "K", "P", "Š"],
        monthNames: ["Sausis", "Vasaris", "Kovas", "Balandis", "Gegužė", "Birželis", "Liepa", "Rugpjūtis", "Rugsėjis", "Spalis", "Lapkritis", "Gruodis"],

    };
    var header = <div style={{ 'textAlign': 'left' }}>
       

        <Accordion>
            <AccordionTab header="Bendra statistika">

                <p>Priimta pacientų: {props.appointmentsCount} </p>

                <p> Vizitų trukmė: {props.totalVisitTime}</p>

            </AccordionTab>
            <AccordionTab header="Detali statistika">

                <label>Pradžios data  </label>
                <Calendar value={props.date1Value} onChange={props.changeDate1Value} maxDate={props.today} inputStyle={{ width: '100px', marginLeft: '10px' }} locale={lt} id="firstDate" placeholder="Pasirinkti" dateFormat="yy-mm-dd" yearRange="1990:2018" monthNavigator="true" yearNavigator="true" />
                <label style={{ marginLeft: '10px' }}> Pabaigos  data  </label>
                <Calendar value={props.date2Value} maxDate={props.today} onChange={props.changeDate2Value} inputStyle={{ width: '100px', marginLeft: '10px' }} locale={lt} id="secondDate" placeholder="Pasirinkti" dateFormat="yy-mm-dd" yearRange="1990:2018" monthNavigator="true" yearNavigator="true" disabled={props.date2Status} />
                <p>{props.specificMessageVisitMessage}</p>
                <p> {props.specificTotalVisitTimeMessage}</p>
            </AccordionTab>
        
          
        </Accordion>
    </div>;
    let brands = [
        {label: '2018', value: 2018},
        {label: '2017', value: 2017},
        {label: '2016', value: 2016},
        {label: '2015', value: 2015},
        {label: '2014', value: 2014},
        {label: '2013', value: 2013},
        {label: '2012', value: 2012},
        {label: '2011', value: 2011},
        {label: '2010', value: 2010},
        {label: '2009', value: 2009}
    ];
    let brandFilter = <Dropdown style={{width: '100%'}} className="ui-column-filter" options={brands}  value = {props.yearsValue} onChange={props.changeYears}/>;


    return (
        <form onChange={props.disableChange}>
            <div>

                <DoctorNavigationComponent history={props.history} />

                <div className="content-section introduction">
                    <DataTable
                        value={props.visitsDataGroupByMonth}
                        header={header}
                        responsive={true}
                        emptyMessage="Įrašų nėra"
                    >
                        <Column style={textAlignStyle} field="3" header = {props.firstHead} filter={true} filterElement={brandFilter}  />
                        <Column style={textAlignStyle} field="1" header="Vizitų skaičius" sortable={true}/>
                        <Column style={textAlignStyle} field="2" header="Vizitams skirtas laikas" sortable={true}  />
                        <Column header={"Išsamiau"} body={props.moreInformationButton} style={{ textAlign: 'center', width: '9em' }} />
                    </DataTable>

                </div>
            </div>
        </form>
    );
};