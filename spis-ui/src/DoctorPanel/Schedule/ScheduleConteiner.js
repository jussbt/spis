import React, { Component } from 'react';
import axios from '../../../node_modules/axios';
import { ScheduleComponent } from './ScheduleComponent';
import { Button } from 'primereact/components/button/Button';
export class ScheduleConteiner extends Component {
    constructor() {
        super();
        this.state = {
            myWorkingData: [],
            firstHead: "2018 metų mėnesis",
            today: { value: new Date() },
            date2Status: true,
            scheduleYearsOptions: [],
            yearValue: 2018,
            date1Value:null,
            
        };
    }
    componentDidMount() {

       
        axios.get('http://localhost:8080/api/doctor/appointmentscount')
            .then((response) => {

                this.setState({
                    appointmentsCount: response.data
                });
            });
        axios.get('http://localhost:8080/api/doctor/getvisittime')
            .then((response) => {

                this.setState({
                    totalVisitTime: response.data
                });
            });
        axios.get('http://localhost:8080/api/doctor/appointmentsgroupedbymonth/2018')
            .then((response) => {

                this.setState({
                    visitsDataGroupByMonth: response.data
                });
            });


    }
    changeDate1Value = (e) => {
        this.setState({
            date1Value: e.value,
            date2Status: false,

        });

    }
    changeDate2Value = (e) => {

        this.setState({
            date2Value: e.value
        });

        if (e.value < this.state.date1Value) {
            let temporalyParameter = this.state.date1Value;
            this.setState({
                date1Value: e.value,
                date2Value: temporalyParameter,
            });
            /** Stupid logic.. I have now idea how this state works.... */
            const dates = {
                firstDate: e.value,
                secondDate: temporalyParameter
            };
            axios.post('http://localhost:8080/api/doctor/getvisittimeSpecific', dates)
                .then((response) => {

                    this.setState({
                        specificVisitTime: response.data,
                        specificMessageVisitTime: "Pasirinkto laikotarpiu , visų vizitų trukmė: " + response.data
                    });
                });
            axios.post('http://localhost:8080/api/doctor/getvisitcountSpecific', dates)
                .then((response) => {
                    this.setState({
                        specificVisitCount: response.data,
                        specificMessageVisitCount: "Pasirinkto laikotarpiu , visi vizitai: " + response.data
                    });
                });
        } else {
            const dates = {
                firstDate: this.state.date1Value,
                secondDate: e.value
            };
            axios.post('http://localhost:8080/api/doctor/getvisittimeSpecific', dates)
                .then((response) => {
                    this.setState({
                        specificVisitTime: response.data,
                        specificMessageVisitTime: "Bendra vizitų trukmė: " + response.data
                    });
                });
            axios.post('http://localhost:8080/api/doctor/getvisitcountSpecific', dates)
                .then((response) => {
                    this.setState({
                        specificVisitCount: response.data,
                        specificMessageVisitCount: "Vizitų skaičius: " + response.data
                    });
                });

        }





    }

    moreInformation = (rowData) => {
        axios.get('http://localhost:8080/api/doctor/appointmentsgroupedbyDay/'+this.state.yearValue+"/"+rowData["0"])
            .then((response) => {

                this.setState({
                    visitsDataGroupByMonth: response.data,
                    disableMoreInformation:true,
                    firstHead:rowData[3] 
                });
            });


    }

    changeYears = (e) => {
        this.setState({
            yearValue: e.value,
            disableMoreInformation:false,
            firstHead:e.value +" metų mėnesis"
        });

        axios.get('http://localhost:8080/api/doctor/appointmentsgroupedbymonth/'+ e.value)
            .then((response) => {
                this.setState({
                    visitsDataGroupByMonth: response.data
                });
            });

    }
   
    disableChange=()=>{
        this.setState({
            disableMoreInformation:false
        });
    }
    render() {

        var moreInformationButton = (rowData) => {
            return <div>
                <Button label="Išsamiau " icon="fal fa-eye" className="ui-button-secondary" onClick={() => this.moreInformation(rowData)}  disabled={this.state.disableMoreInformation}/>
            </div>;
        };
       
        return (
           
            <div>
                <ScheduleComponent
                    maxriba={this.state.maxriba}
                    date2Status={this.state.date2Status}
                    date1Value={this.state.date1Value}
                    date2Value={this.state.date2Value}
                    changeDate1Value={this.changeDate1Value}
                    changeDate2Value={this.changeDate2Value}
                    today={this.state.today.value}
                    moreInformationButton={moreInformationButton}
                    monthHeader3={this.state.monthHeader3}
                    appointmentsCount={this.state.appointmentsCount}
                    totalVisitTime={this.state.totalVisitTime}
                    visitsDataGroupByMonth={this.state.visitsDataGroupByMonth}
                    specificTotalVisitTimeMessage={this.state.specificMessageVisitTime}
                    specificMessageVisitMessage={this.state.specificMessageVisitCount}
                    scheduleYearsOptions={this.state.scheduleYearsOptions}
                    yearValue={this.state.yearValue}
                    yearsValue={this.state.yearValue}
                    changeYears={this.changeYears}
                    disableChange={this.disableChange}
                    firstHead={this.state.firstHead}
                    history={this.props.history}

                />
            </div>
        );
    }
}