import React from 'react';
import { NavLink } from 'react-router-dom';
import { Toolbar } from 'primereact/components/toolbar/Toolbar';
import { Button } from 'primereact/components/button/Button';
import LogOutComponent from '../../LoginLogout/LogOutComponent';
import logo from '../../logo.png';

export var DoctorNavigationComponent = (props) => {
    return (
        <Toolbar>
            <div className="ui-toolbar-group-left">
               
                <img src={logo} alt='' height="30px" width='auto'/>
                
                <NavLink to="/doctor/patients">
                    <Button label="Mano pacientai" icon="fa-folder-open" />
                </NavLink>

                <NavLink to="/doctor/schedule">
                    <Button label="Darbo grafikas" icon="fas fa-calendar" className="ui-button-warning" />
                </NavLink>
            </div>
            <div className="ui-toolbar-group-right">
                <NavLink to="/changepassword">
                    <Button label="Slaptažodžio keitimas" icon="fa-check" className="ui-button-warning" />
                </NavLink>
                <LogOutComponent history={props.history} />
            </div>
        </Toolbar>
    );
};