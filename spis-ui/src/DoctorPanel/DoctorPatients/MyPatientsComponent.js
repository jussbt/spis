import React from 'react';
import { DataTable } from 'primereact/components/datatable/DataTable';
import { Column } from 'primereact/components/column/Column';
import { DoctorNavigationComponent } from '../DoctorNavigation/DoctorNavigationComponent';
import { Sidebar } from 'primereact/components/sidebar/Sidebar';
import { TabView, TabPanel } from 'primereact/components/tabview/TabView';

export var MyPatientsComponent = (props) => {

    const textAlignStyle = {
        textAlign: 'center'
    };

    return (
        <div>
            <DoctorNavigationComponent history={props.history} />
            <Sidebar style={{width:'20em'}}  visible={props.visibleRight} position="right" baseZIndex={1000000} onHide={props.onHide}>
                {props.register}
            </Sidebar>
            <Sidebar style={{width:'20em'}} visible={props.visibleRightPrescription} position="right" baseZIndex={1000000} onHide={props.onHidePrescription}>
                {props.registerPrescription}
            </Sidebar>

            <div className="content-section introduction">
                <TabView>
                    <TabPanel header="Priskirti pacientai" leftIcon="fa-folder-open">
                        <DataTable  ref={props.exportCVFref} value={props.patients} header={props.header} paginator={true} responsive={true}  rows={50} rowsPerPageOptions={[70,100,150]}
                            globalFilter={props.globalFilter} emptyMessage="Įrašų nėra" >
                            
                            <Column style={textAlignStyle} field="lastName" header="Pavardė"  />
                            <Column style={textAlignStyle} field="firstName" header="Vardas" />
                            <Column style={textAlignStyle} field="personalCode" header="Asmens kodas" />
                            <Column style={textAlignStyle} field="birthDate" header="Gimimo data" />
                            
                            <Column  header={"Vizitas"} body={props.bodyVisit} style={{ textAlign: 'center', width: '15em' }} />
                            <Column  header={"Receptas"} body={props.bodyPrescription} style={{ textAlign: 'center', width: '15em' }} />
                            <Column  header={"Info"} body={props.bodyHistory} style={{ textAlign: 'center', width: '15em' }} />
                           
                        </DataTable>
                    </TabPanel>

                    <TabPanel header="Kiti pacientai" rightIcon="fa-folder-open">
                        <DataTable  responsive={true} value={props.specificPatient} header={props.allPatientsHeader} emptyMessage="Įrašų nėra">                          
                            <Column style={textAlignStyle} field="lastName" header="Pavardė" />
                            <Column style={textAlignStyle} field="firstName" header="Vardas" />
                            <Column style={textAlignStyle} field="personalCode" header="Asmens kodas" />
                            <Column header={"Vizitas"} body={props.bodyVisit} style={{ textAlign: 'center', width: '15em' }} />
                            <Column header={"Receptas"} body={props.bodyPrescription} style={{ textAlign: 'center', width: '15em' }} />
                        </DataTable>
                    </TabPanel>

                </TabView>
            </div>
            {props.bodyMessage}
        </div>
    );
};
