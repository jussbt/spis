import React from 'react';
import { DoctorNavigationComponent } from '../DoctorNavigation/DoctorNavigationComponent';
import ValidPrescriptionsComponent from '../../PatientPanel/ValidPrescriptionsComponent';

const PatientPrescriptionsComponent = (props) => {
    return (
        <div>
            <DoctorNavigationComponent history={props.history} />
            <ValidPrescriptionsComponent
                selectHandler={props.selectHandler} 
                dialogOpen={props.dialogOpen}
                dialogClose={props.dialogClose}
                prescriptions={props.prescriptions}
                activePage={props.activePage}
                totalItemsCount={props.totalItemsCount}
                totalPages={props.totalPages}
                onChange={props.onChange}
                purchases={props.purchases}
                description={props.description}
                ingredient={props.ingredient}
            />
        </div>
    );
};

export default PatientPrescriptionsComponent;