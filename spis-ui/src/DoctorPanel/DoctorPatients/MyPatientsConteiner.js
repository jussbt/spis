import React, { Component } from 'react';
import axios from '../../../node_modules/axios';
import { MyPatientsComponent } from './MyPatientsComponent';
import { InputText } from 'primereact/components/inputtext/InputText';
import { Button } from 'primereact/components/button/Button';
import { Dropdown } from 'primereact/components/dropdown/Dropdown';
import { AddDiseaseConteiner } from '../addDesease/AddDiseaseConteiner';
import { AddPrescriptionConteiner } from '../addPrescription/AddPrescriptionConteiner';
import { Messages } from 'primereact/components/messages/Messages';


axios.defaults.withCredentials = true; 

export class MyPatientsConteiner extends Component {
    constructor(props) {
        super(props);
        this.state = {
            patients: [],
            allPatients: [],
            visibleRight: false,
            visibleRightPrescription: false,
            specificPatientData: [],
            dropdownSelection:  {},
            findPatientValue:0
        };
        this.export = this.export.bind(this);
    }

    componentDidMount = () => {
        axios.get('http://localhost:8080/api/doctor/patients')
            .then((response) => {
                this.setState({
                    patients: response.data

                });
            });
    }

    export() { 
        this.dt.exportCSV();    
    }

    buttonClick = (rowData) => {
        this.setState({
            specificPatientData: rowData,
            visibleRight: true
        });
    }

    registerPrescription = (rowData) => {
        this.setState({
            specificPatientData: rowData,
            visibleRightPrescription: true
        });
    }

    bodyVisit = (rowData, column) => {
        return <div>
            <Button label="Pridėti ligos įrašą" className="ui-button-secondary" icon="fa-plus" onClick={() => this.buttonClick(rowData)} />         
        </div>;
    }

    bodyPrescription = (rowData, column) => {
        return <div>
            <Button label="Pridėti receptą " icon="fa-plus" className="ui-button-secondary" onClick={() => this.registerPrescription(rowData)} />
        </div>;
    }

    bodyHistory = (rowData, column) => {
        return <div>
            <Dropdown 
                value={this.state.dropdownSelection} 
                options={ [ {label: 'Ligos istorija', value: 'istorija'}, {label: 'Receptai', value: 'receptai'} ] }
                onChange={(e) => this.onDropdownChange(rowData, e)}
                placeholder="Pasirinkti"
            />
        </div>;
    }

    onDropdownChange = (rowData, e) => {
        e.value === 'istorija' ? this.props.history.push('/doctor/patientappointments/' + rowData.uuid) :
            this.props.history.push('/doctor/patientprescriptions/' + rowData.uuid);
    }

    onHide = () => {
        this.setState({
            visibleRight: false,
            visibleRightPrescription: false
        });
    }

    findPatient = () => {
        if(this.state.findPatientValue==0){
            this.messages.show({ severity: 'error', summary: 'Neįvestas asmens kodas', life: 5000 }); 
            return;
        }    
        if(this.state.findPatientValue.length<11){
            this.messages.show({ severity: 'error', summary: 'Asmens kodas - 11 skaitmenų', life: 5000 }); 
            return;
        }  
        axios.get("http://localhost:8080/api/doctor/findpatients/" + this.state.findPatientValue)
            .then(response => {
                if(response.data.length==0){
                    this.messages.show({ severity: 'error', summary: 'Pacientas nerastas', life: 5000 }); 
                }  else{ 
                    this.setState({
                        specificPatient: response.data
                    });
                }
            })
            .catch(() => {
                
            });
    }

    render() {
        /**Search header */
        var header =
            <div>
                <div style={{ 'textAlign': 'left' }}>
                    <i className="fa fa-search" style={{ margin: '4px 4px 0 0' }}></i>
                    <InputText type="search" onInput={(e) => this.setState({ globalFilter: e.target.value })} placeholder="Paieška tarp pacientų" size="50" />                    
                    <Button type="button" label="Generuoti pacientų CSV failą" onClick={this.export} style={{marginLeft: '10px'}}></Button> 
                </div>
            </div>;
        var allPatientsHeader =
        <div style={{ 'textAlign': 'left' }}>
            <i className="fa fa-search" style={{ margin: '4px 4px 0 0' }}></i>
            <InputText  keyfilter="int" onInput={(e) => this.setState({ findPatientValue: e.target.value })} placeholder="Paciento paieška pagal asmens kodą" size="50" maxlength="11" />
            <Button label="Surasti " onClick={this.findPatient} style={{marginLeft: '10px'}}/>     
        </div>;
        let bodyMessages =
        <div>
            <Messages ref={(el) => { this.messages = el; }} style={{margin: '0 auto'}}></Messages>
        </div>;
 
        return (
            <div className="content-section implementation">

                <MyPatientsComponent
                    register={<AddDiseaseConteiner onHide={this.onHide}
                        patientName={this.state.specificPatientData.firstName} patientLastName={this.state.specificPatientData.lastName}
                        patientId={this.state.specificPatientData.uuid}
                    />}
                    
                    registerPrescription={<AddPrescriptionConteiner
                        onHide={this.onHide}
                        patientName={this.state.specificPatientData.firstName} patientLastName={this.state.specificPatientData.lastName}
                        patientId={this.state.specificPatientData.uuid}
                    />}
                    exportCVFref={(el) => { this.dt = el; }}
                    globalFilter={this.state.globalFilter}
                    visibleRightPrescription={this.state.visibleRightPrescription}
                    history={this.props.history}
                    visibleRight={this.state.visibleRight}
                    bodyVisit={this.bodyVisit}
                    bodyPrescription={this.bodyPrescription}
                    bodyHistory={this.bodyHistory}
                    header={header}
                    onHide={this.onHide}
                    onHidePrescription={this.onHide}
                    patients={this.state.patients}
                    specificPatient={this.state.specificPatient}
                    allPatientsHeader={allPatientsHeader} 
                    bodyMessage={bodyMessages}/>
            </div>
        );
    }
}

