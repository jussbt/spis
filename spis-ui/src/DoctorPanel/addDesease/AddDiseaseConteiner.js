import React from 'react';
import axios from 'axios';
import { Messages } from 'primereact/components/messages/Messages';

import { AddDeseaseComponent } from './AddDeseaseComponent';


export class AddDiseaseConteiner extends React.Component {
    constructor() {
        super();
        
        this.state = {
            visitDuration: null,
            diseaseCode: '',
            visitDescriptionValue: null,
            visitFree: false,
            visitRepeat: false,
            diseaseList: [],          
            dropDownValue: ''
        };
     
    }
    changeDuration = (e) => {
        this.setState({
            visitDuration: e.value
        });
    }
    visitDescriptionChange = (e) => {

        if (e.target.value.length>249){
            this.messages.show({ severity: 'error', summary: 'Maksimalus vizito aprašymo limitas', life: 5000 });
            return false;
        }
        this.setState({
            visitDescriptionValue: e.target.value
        });
    }
    onVisitFreeChange = (e) => {
        this.setState({
            visitFree: e.value
        });
    }
    onVisitRepeatChange = (e) => {
        this.setState({
            visitRepeat: e.value
        });
    }

    dropdownValuesChange = (e) => {
        if (e.value.length > 1) {
            axios.get('http://localhost:8080/api/diseases/' + e.value)
                .then((response) => {
                    const diseases = response.data.map(disease1 => {
                        return { label: disease1.code + " : " + disease1.description, value: disease1.id };
                    });
                    this.setState({
                        diseaseList: diseases,
                    });
                });
        } else {
            this.setState({
                diseaseList: []
            });

        }
        this.setState({            
            dropDownValue: e.value
        });
    }

    register = (e) => {
        if (this.state.dropDownValue === null) {        
            this.messages.show({ severity: 'error', summary: 'Nepasirinktas ligos kodas', life: 5000 });        
            return;
        }
        if (this.state.visitDuration === null) {
            this.messages.show({ severity: 'error', summary: 'Nepasirinkta vizito trukmė', life: 5000 });
            return;
        }
        if (this.state.visitDescriptionValue.length <1) {
            this.messages.show({ severity: 'error', summary: 'Vizitas nebuvo aprašytas', life: 5000 });
            return;
        }

        const disease = {
            description: this.state.visitDescriptionValue,
            diseaseId: this.state.dropDownValue,
            duration: this.state.visitDuration,
            patientUuid: this.props.patientId,
            compensated: this.state.visitFree,
            repeated: this.state.visitRepeat
        };
        axios.post("http://localhost:8080/api/appointment/create", disease)

            .then(() => {
                this.setState({                   
                    visitDuration: null,
                    visitDescriptionValue:'',         
                    errorMessage: "",
                    diseaseList:[]
                });
                this.messages.show({ severity: 'success', summary: 'Vizitas sėkmingai užregistruotas', life: 5000 });
            })
            .catch(() => {
                
            });
        e.preventDefault();
    }

    onHide = () => {
        this.setState({
            visitDuration: null,
            visitDescriptionValue: '',
            visitFree: false,
            visitRepeat: false
        });
        this.props.onHide();
    }
    
    

    render() {

        let bodyMessages =
        <div>
            <Messages ref={(el) => { this.messages = el; }} style={{margin: '0 auto'}}></Messages>
        </div>; 

        var duration = [
            { label: '10 min', value: '10' },
            { label: '15 min', value: '15' },
            { label: '20 min', value: '20' },
            { label: '25 min', value: '25' },
            { label: '30 min', value: '30' },
            { label: '35 min', value: '35' },            
            { label: '40 min', value: '40' },
            { label: '45 min', value: '45' },            
            { label: '50 min', value: '50' },
            { label: '55 min', value: '55' },            
            { label: '60 min', value: '60' }
        ];
        return (
            <AddDeseaseComponent
                diseases={this.state.diseaseList}                
                onHide={this.onHide}
                patientName={this.props.patientName}
                patientLastName={this.props.patientLastName}
                duration={duration}
                changeDuration={this.changeDuration}
                visitDurationValue={this.state.visitDuration}
                visitDescriptionChange={this.visitDescriptionChange}
                visitDescriptionValue={this.state.visitDescriptionValue}

                checkedFree={this.state.visitFree}
                onVisitFreeChange={this.onVisitFreeChange}
                onVisitRepeatChange={this.onVisitRepeatChange}
                checkedRepeat={this.state.visitRepeat}
                successMessage={this.state.successMessage}
                errorMessage={this.state.errorMessage}
                dropDownValue={this.state.dropDownValue}
                dropdownValuesChange={this.dropdownValuesChange}
                register={this.register}
                changeSuccessMessage={this.changeSuccessMessage}
                bodyMessages={bodyMessages}
                
            />
        );
    }
}