import { InputTextarea } from 'primereact/components/inputtextarea/InputTextarea';
import React from 'react';
import { Dropdown } from 'primereact/components/dropdown/Dropdown';
import { ToggleButton } from 'primereact/components/togglebutton/ToggleButton';
import { Button } from 'primereact/components/button/Button';

export var AddDeseaseComponent = (props) => {

    return (
        <form onChange={props.changeSuccessMessage}>
            <div className="form-group">
                <Dropdown id="visitDuration"   value={props.visitDurationValue} options={props.duration} onChange={props.changeDuration} style={{ width: '220px' }} placeholder="Vizito trukmė"  />
            </div>
            <div className="form-group">           
                <Dropdown
                    value={props.dropDownValue}
                    options={props.diseases} onChange={props.dropdownValuesChange} style={{ width: '220px' }} placeholder="Ligos kodas"
                    filter={false}
                    filterPlaceholder="Ieškoti"
                    filterBy="label,value"
                    id="TLKcode"
                    editable
                />
            </div>
            <div className="form-group">
                <InputTextarea value={props.visitDescriptionValue} onChange={props.visitDescriptionChange} cols={25} rows={5} id="visitDesc" placeholder="Vizito aprašymas"  autoResize={true} maxlength={250}/>
            </div>
            <div className="form-group">
                <p>Ar vizitas yra kompensuojamas?</p>
                <ToggleButton style={{ width: '110px' }} onLabel="Taip" offLabel="Ne" onIcon="fa-check-square" offIcon="fa-square"
                    checked={props.checkedFree} onChange={props.onVisitFreeChange} id="visitFree" />
            </div>
            <div className="form-group">
                <p>Ar vizitas yra pakartotinis?</p>
                <ToggleButton style={{ width: '110px' }} onLabel="Taip" offLabel="Ne" onIcon="fa-check-square" offIcon="fa-square"
                    checked={props.checkedRepeat} onChange={props.onVisitRepeatChange} id="visitRepeat" />
            </div>
            <div className="form-group">
                <label>Pacientas: {props.patientName + " " + props.patientLastName} </label>
            </div>
        
            <div className="form-group">
                <Button label="Registruoti" onClick={props.register} className="ui-button-success" id="registerVisit" />
                <Button label="Atšaukti" onClick={props.onHide} className="ui-button-danger" id="cancelVisit" />           
               
           
            </div>
            <div className="ui-toolbar-group-right">
                {props.bodyMessages}
            </div>

        </form >
    );
};