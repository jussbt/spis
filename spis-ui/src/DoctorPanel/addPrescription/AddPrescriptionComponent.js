import React from 'react';
import { Button } from 'primereact/components/button/Button';
import { InputText } from 'primereact/components/inputtext/InputText';
import { InputTextarea } from 'primereact/components/inputtextarea/InputTextarea';
import { Dropdown } from 'primereact/components/dropdown/Dropdown';
import { Calendar } from 'primereact/components/calendar/Calendar';
import {ToggleButton} from 'primereact/components/togglebutton/ToggleButton';

export var AddPrescriptionComponent = (props) => {
   
    let lt = {
        firstDayOfWeek: 1,
        dayNames: ["Sekmadienis","Pirmadienis", "Antradienis", "Trečiadienis", "Ketvirtadienis", "Penktadienis", "Šeštadienis"],
        dayNamesShort: ["sek","pirm", "antr", "treč", "ketv", "penk", "šeš"],
        dayNamesMin: ["S","P", "A", "T", "K", "P", "Š"],
        monthNames: ["Sausis", "Vasaris", "Kovas", "Balandis", "Gegužė", "Birželis", "Liepa", "Rugpjūtis", "Rugsėjis", "Spalis", "Lapkritis", "Gruodis"],

    };
 
    return (
        <form onChange={props.cleanMessages}>
            
            <div className="form-group" style={{display: 'block'}}>
                <ToggleButton style={{ width: '220px'}} onLabel="Receptas neturi pabaigos datos" offLabel="Receptas turi galiojimo pabaigą" onIcon="fa-check-square" offIcon="fa-square"
                    checked={props.expChecked} onChange={props.expCheckedChange} id="PrescExpirationToggle" />
                <br />
                <Calendar inputStyle={{ width: '220px',marginTop:'10px'}}  locale={lt} id="presExpiration" placeholder="Recepto galiojimo pabaiga" dateFormat="yy-mm-dd" minDate={new Date()} yearRange="2018:2020" monthNavigator="true" yearNavigator="true" readOnlyInput={true} value={props.presExp} onChange={props.presExpChange} disabled={props.prescExpVisible}  />
            </div>
            <div className="form-group">                          
                <Dropdown id="drug" value={props.drugValue} options={props.drugs} onChange={props.changeDrug} style={{ width: '220px' }} placeholder="Veiklioji medžiaga"
                    filter={true}
                    filterPlaceholder="Ieškoti"
                    filterBy="label,value"
                    required 
                />       
            </div>

            <div className="form-group">                
                <InputText  keyfilter="pnum" id="amount" value={props.amount}  onChange={props.onAmountChange} placeholder="kiekis" style={{ width: '90px' }}  required/>
                <Dropdown  required id="drug" value ={props.unitsValue} options={props.units} onChange={props.changeUnits} style={{ width: '120px',marginLeft:'10px' }} placeholder="vienetai" />
            </div>

            <div className="form-group">
                <InputTextarea value={props.presDescriptionValue} onChange={props.presDescriptionChange} cols={25} rows={5} id="prescriptionDesc"  placeholder="Vartojimo aprašymas" autoResize={true} maxlength={250}/>
            </div>

            <div className="form-group">
                <label>Pacientas: {props.patientName + " " + props.patientLastName} </label>
            </div>
            
            <div className="form-group">
                <Button label="Registruoti" onClick={props.register} className="ui-button-success" id="registerVisit" />
                <Button label="Atšaukti" onClick={props.onHide} className="ui-button-danger" id="cancelVisit" />         
            </div>

            <div className="ui-toolbar-group-right">
                {props.bodyMessages}
            </div>

        </form>
    );
};