import React from 'react';
import { AddPrescriptionComponent } from './AddPrescriptionComponent';
import axios from '../../../node_modules/axios';
import { Messages } from 'primereact/components/messages/Messages';

export class AddPrescriptionConteiner extends React.Component {

    constructor() {
        super();
        this.state = {
            drugsList: [],
            presDescriptionValue: '',
            drug: null,
            unitsValue: null,
            presExp: "",
            amount: 0,
            expChecked: true,
            errorMessage: "",
            successMessage: ""
        };
    }

    componentDidMount = () => {
        axios.get('http://localhost:8080/api/drugs')
            .then((response) => {
                const drugs = response.data.map(drug => {
                    return { label: drug.activeIngredient, value: drug.activeIngredient };
                });
                this.setState({
                    drugsList: drugs
                });
            });
    }

    changeDrug = (e) => {
        this.setState({
            drug: e.value
        });
    }

    onAmountChange = (e) => {
        this.setState({
            amount: e.target.value
        });
    }

    presDescriptionChange = (e) => {
        this.setState({
            presDescriptionValue: e.target.value
        });
    }

    changeUnits = (e) => {
        this.setState({
            unitsValue: e.value
        });
    }

    presExpChange = (e) => {
        this.setState({
            presExp: e.value
        });
    }

    expCheckedChange = (e) => {
        this.setState({
            presExp: "",
            expChecked: e.value
        });
    }


    register = (e) => {
        if (this.state.drug === null) {
            this.messages.show({ severity: 'error', summary: 'Nepasirinktas vaistas', life: 1500 });
            return;
        }
        if (this.state.amount === 0) {
            this.messages.show({ severity: 'error', summary: 'Nepasirinktas kiekis', life: 2000 });
            return;
        }
        if (this.state.amount < 0) {
            this.messages.show({ severity: 'error', summary: "Veikliosios medžiagos kiekis - tik teigiamas.", life: 2000 });
            return;
        }
        if (this.state.unitsValue === null) {
            this.messages.show({ severity: 'error', summary: "Nepasirinkti vienetai", life: 2000 });
            return;
        }
        if (this.state.presDescriptionValue.length < 1) {
            this.messages.show({ severity: 'error', summary: 'Vartojimas nebuvo aprašytas', life: 4000 });
            return;
        }
        const prescriptionToRegister = {
            amount: this.state.amount,
            description: this.state.presDescriptionValue,
            drugMaterial: this.state.drug,
            expirationDate: this.state.presExp,
            patientUuid: this.props.patientId,
            units: this.state.unitsValue
        };
        axios.post("http://localhost:8080/api/prescriptions/create", prescriptionToRegister)
            .then(() => {
                this.setState({
                    expChecked: true,
                    unitsValue: null,
                    presDescriptionValue: '',
                    presExp: "",
                    amount: 0,
                    drug: null
                });
                this.messages.show({ severity: 'success', summary: 'Receptas sėkmingai užregistruotas', life: 5000 });
            })
            .catch(() => {
                
            });
        e.preventDefault();
    }
 
    onHide = () => {
        this.setState({
            expChecked: true,
            unitsValue: null,
            presDescriptionValue: '',
            presExp: "",
            amount: 0,
            drug: null
        });
        this.props.onHide();
    }

    render() {

        let bodyMessages =
            <div>
                <Messages ref={(el) => { this.messages = el; }} style={{margin: '0 auto'}}></Messages>
            </div>;
    

        var units = [
            { label: 'mg', value: 'mg' },
            { label: 'µg', value: ' µg' },
            { label: 'TV/IU', value: 'TV/IU' }
        ];
        return (<AddPrescriptionComponent
            patientName={this.props.patientName}
            patientLastName={this.props.patientLastName}
            onHide={this.onHide}
            drugs={this.state.drugsList}
            changeDrug={this.changeDrug}
            drugValue={this.state.drug}
            presDescriptionChange={this.presDescriptionChange}
            presDescriptionValue={this.state.presDescriptionValue}
            amount={this.state.amount}
            onAmountChange={this.onAmountChange}
            unitsValue={this.state.unitsValue}
            units={units}
            changeUnits={this.changeUnits}
            prescExpVisible={this.state.expChecked}
            expChecked={this.state.expChecked}
            expCheckedChange={this.expCheckedChange}
            register={this.register}
            presExp={this.state.presExp}
            presExpChange={this.presExpChange}
            successMessage={this.state.successMessage}
            errorMessage={this.state.errorMessage}
            cleanMessages={this.cleanMessages}
            bodyMessages={bodyMessages}
        />);
    }
}