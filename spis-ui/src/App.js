import React from 'react';
import { Login } from './LoginLogout/Login';
import { Route, Switch, HashRouter,Redirect } from 'react-router-dom';
import { AdminCreatesWindow } from './Admin/AdminCreatesWindow';
import { AdminNavigation } from './Admin/AdminNavigation';
import { AllPAtientsConteiner } from './Patients/AllPatientsConteiner';
import { MyPatientsConteiner } from './DoctorPanel/DoctorPatients/MyPatientsConteiner';
import { AllDoctorsConteiner} from './Doctors/AllDoctorsConteiner';
import PatientAppointmentsContainer from './DoctorPanel/DoctorPatients/PatientAppointmentsContainer';
import PatientPrescriptionsContainer from './DoctorPanel/DoctorPatients/PatientPrescriptionsContainer';
import AppointmentsContainer from './PatientPanel/AppointmentsContainer';
import PrescriptionsContainer from './PatientPanel/PrescriptionsContainer';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {injector} from 'react-services-injector';
import services from './services';
import MyPharmacistComponent from './Pharmacist/PharmacistNavigationComponent';
import PharmacistContainer from './Pharmacist/PharmacistContainer';
import PasswordChangeComponent from './Password/PasswordChangeComponent';
import HomepageComponent from './Homepage/HomepageComponent';
import { ScheduleConteiner } from './DoctorPanel/Schedule/ScheduleConteiner';

injector.register(services);
const {UserStorage} = injector.get();

const font = "'Roboto','Trebuchet MS','Arial','Helvetica','sans-serif'";
const muiTheme = getMuiTheme({
    fontFamily: font
});
 
class App extends React.Component {
    render() {
        const PrivateAdminRoute = ({ component: Component, ...rest }) => (
            <Route {...rest} render={(props) => (
                atob(UserStorage.currentRole) === 'kaspa'
                    ? <Component {...props} />
                    : <Redirect to='/login' />
            )} />
        );
        const PrivateDoctorRoute = ({ component: Component, ...rest }) => (
            <Route {...rest} render={(props) => (
                atob(UserStorage.currentRole) === 'ignas'
                    ? <Component {...props} />
                    : <Redirect to='/login' />
            )} />
        );
        const PrivatePatientRoute = ({ component: Component, ...rest }) => (
            <Route {...rest} render={(props) => (
                atob(UserStorage.currentRole) === 'algis'
                    ? <Component {...props} />
                    : <Redirect to='/login' />
            )} />
        );
        const PrivatePharmacistRoute = ({ component: Component, ...rest }) => (
            <Route {...rest} render={(props) => (
                atob(UserStorage.currentRole) === 'justas'
                    ? <Component {...props} />
                    : <Redirect to='/login' />
            )} />
        );
        
        return (
            <HashRouter>
                <div className="container">
                    <Switch>
                        <Route exact path="/" component={HomepageComponent} />
                        <Route exact path="/login" component={Login} /> 
                        <PrivateAdminRoute exact path='/admin' component={AdminNavigation} />
                        <PrivateAdminRoute exact path="/admin/new" component={AdminCreatesWindow} />
                        <PrivateAdminRoute exact path="/admin/doctors" component={AllDoctorsConteiner} />                        
                        <PrivateAdminRoute exact path="/admin/patients/:userId" component={AllPAtientsConteiner} />
                        <PrivateDoctorRoute exact path="/doctor" component={MyPatientsConteiner} />
                        <PrivateDoctorRoute exact path="/doctor/patients" component={MyPatientsConteiner} />                    
                        <PrivateDoctorRoute exact path="/doctor/schedule" component={ScheduleConteiner} />                         
                        <Route exact path="/changepassword" component={PasswordChangeComponent} />                  
                        <MuiThemeProvider muiTheme={muiTheme}>
                            <div style={{fontFamily: font}}>
                                <PrivatePharmacistRoute exact path="/pharmacist" component={MyPharmacistComponent} />
                                <PrivatePharmacistRoute exact path="/pharmacist/prescriptions" component={PharmacistContainer} />
                                <PrivatePatientRoute exact path="/patient/appointments" component={AppointmentsContainer} />
                                <PrivatePatientRoute exact path="/patient/prescriptions" component={PrescriptionsContainer} />
                                <PrivateDoctorRoute exact path="/doctor/patientappointments/:userId" component={PatientAppointmentsContainer} />
                                <PrivateDoctorRoute exact path="/doctor/patientprescriptions/:userId" component={PatientPrescriptionsContainer} /> 
                            </div>
                        </MuiThemeProvider>
                    </Switch>
                </div>
            </HashRouter>
        );
    }
}

export default App;
