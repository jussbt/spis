import React from 'react';
import axios from 'axios';
import PrescriptionsStatisticsComponent from './PrescriptionsStatisticsComponent';

class PrescriptionsStatisticsContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            purchases: [],
            top10Drugs: {},
            activeIngredient: []
        };
    }

    componentDidMount() {
        axios.get('http://localhost:8080/api/drugs/statistics')
            .then(response => {
                let dynamicColumns = response.data.top10Drugs.map(col => {
                    return {
                        activeIngredient: col.activeIngredient,
                        count: col.count,
                    };
                });
                const labels = response.data.top10Drugs.map(drug => {
                    return drug.activeIngredient;
                });
                const data = response.data.top10Drugs.map(drug => {
                    return drug.count;
                });
                this.setState({
                    purchases: dynamicColumns,
                    top10Drugs: data,
                    activeIngredient: labels
                });
            })
            .catch(() => {

            });
    }

    render() {
        return <PrescriptionsStatisticsComponent 
            data={this.state.top10Drugs}
            labels={this.state.activeIngredient}
            purchases={this.state.purchases}
        />;
    }
}

export default PrescriptionsStatisticsContainer;