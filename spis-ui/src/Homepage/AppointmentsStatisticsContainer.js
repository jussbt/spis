import React from 'react';
import axios from 'axios';
import AppointmentsStatisticsComponent from './AppointmentsStatisticsComponent';

class AppointmentsStatisticsContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            appointments: [],
            top10Diseases: {},
            descriptions: []
        };
    }

    componentDidMount() {
        axios.get('http://localhost:8080/api/diseases/statistics')
            .then(response => {
                let dynamicColumns = response.data.top10Diseases.map(col => {
                    return {
                        code: col.code,
                        count: col.count,
                        percent: (col.count * 100 / response.data.totalAppoinments).toFixed(2)
                    };
                });
                const descriptions = response.data.top10Diseases.map(disease => {
                    return disease.description;
                });
                const data = response.data.top10Diseases.map(disease => {
                    return disease.count;
                });
                this.setState({
                    appointments: dynamicColumns,
                    top10Diseases: data,
                    descriptions: descriptions
                });
            })
            .catch(() => {

            });
    }

    render() {
        return (   
            <AppointmentsStatisticsComponent 
                data={this.state.top10Diseases}
                descriptions={this.state.descriptions}
                appointments={this.state.appointments} 
            />
        );
    }
}

export default AppointmentsStatisticsContainer;