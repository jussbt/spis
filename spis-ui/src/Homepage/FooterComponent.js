import React from 'react';
import { Toolbar } from 'primereact/components/toolbar/Toolbar';

const FooterComponent = () => {
    return (
        <Toolbar style={{marginTop: '20px', color: '#757575'}}>
            <div className="ui-toolbar-group-right">
                Copypyright © 2018 Infobalt Tech City akademija
            </div>
        </Toolbar>
    );
};

export default FooterComponent;