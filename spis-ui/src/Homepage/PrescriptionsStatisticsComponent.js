import React from 'react';
import {Card} from 'primereact/components/card/Card';
import { Chart } from 'primereact/components/chart/Chart';
import {DataTable, Column} from 'primereact/components/datatable/DataTable';
import './homepage.css';

const PrescriptionsStatisticsComponent = (props) => {
    
    var data = {
        labels: props.labels,
        datasets: [
            {
                label: 'Pirkimų skaičius',
                backgroundColor: '#4BC0C0',
                borderColor: '#4BC0C0',
                data: props.data
            }
        ]    
    };

    var options = {
        responsive: true,
        legend: {
            position: 'bottom',
            labels: {
                boxWidth: 60
            }
        },
        layout: {
            padding: {
                left: 25,
                right: 0,
                top: 25,
                bottom: 0
            }
        },
        tooltips: {
            intersect: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    suggestedMin: 0,
                    suggestedMax: 50
                }
            }],
            xAxes: [{
                display: false
            }]
        }
    };

    const textAlignStyle = {
        textAlign: 'center'
    };

    return (
        <div>
            <Card className='whole-card card-title' title='Vaistų statistika' style={{marginTop: '20px'}}>
                <Card style={{display: 'inline-block', width: '59%', height: '371px', margin: '5px'}}>
                    <Chart type='bar' data={data} options={options}/>
                </Card>
                <Card style={{display: 'inline-block', width: '39%', marginLeft: '5px'}}>
                    <DataTable value={props.purchases} emptyMessage="Įrašų nėra">
                        <Column field="activeIngredient" header="Veiklioji medžiaga" style={textAlignStyle}/>
                        <Column field="count" header="Pirkimų skaičius" style={textAlignStyle}/>
                    </DataTable>
                </Card>
            </Card>
        </div>
    );
};

export default PrescriptionsStatisticsComponent;