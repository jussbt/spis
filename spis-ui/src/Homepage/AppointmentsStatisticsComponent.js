import React from 'react';
import {Card} from 'primereact/components/card/Card';
import { Chart } from 'primereact/components/chart/Chart';
import { DataTable, Column } from 'primereact/components/datatable/DataTable';
import './homepage.css';

const AppointmentsStatisticsComponent = (props) => {

    var data = {
        labels: props.descriptions,
        datasets: [
            {
                label: 'Susirgimų skaičius',
                backgroundColor: '#42A5F5',
                borderColor: '#1E88E5',
                data: props.data
            }
        ]    
    };

    var options = {
        responsive: true,
        legend: {
            position: 'bottom',
            labels: {
                boxWidth: 60
            }
        },
        tooltips: {
            intersect: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    suggestedMin: 0,
                    suggestedMax: 50
                }
            }],
            xAxes: [{
                display: false
            }]
        }
    };

    const textAlignStyle = {
        textAlign: 'center'
    };

    return (
        <div>
            <Card className='whole-card card-title' title='Ligų statistika' style={{marginTop: '20px'}}>
                <Card className='left-card' style={{display: 'inline-block', width: '59%', height: '371px', marginRight: '5px', marginLeft: '5px'}}>
                    <Chart type='bar' data={data} options={options}/>
                </Card>
                <Card style={{display: 'inline-block', width: '39%', marginLeft: '5px'}}>
                    <DataTable value={props.appointments} emptyMessage="Įrašų nėra">
                        <Column field="code" header="TLK-10 kodas" style={textAlignStyle}/>
                        <Column field="count" header="Susirgimų skaičius" style={textAlignStyle}/>
                        <Column field="percent" header="Dalis visų susirgimų, %" style={textAlignStyle}/>
                    </DataTable>
                </Card>
            </Card>
        </div>
    );
};

export default AppointmentsStatisticsComponent;