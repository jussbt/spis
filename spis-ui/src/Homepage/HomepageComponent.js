import React from 'react';
import { Toolbar } from 'primereact/components/toolbar/Toolbar';
import { Button } from 'primereact/components/button/Button';
import { NavLink } from 'react-router-dom';
import logo from '../logo.png';
import AppointmentsStatisticsContainer from './AppointmentsStatisticsContainer';
import PrescriptionsStatisticsContainer from './PrescriptionsStatisticsContainer';
import FooterComponent from './FooterComponent';
import './homepage.css';

const HomepageComponent = () => {  
    return (
        <div>
            <Toolbar>
                <div className="ui-toolbar-group-left">
                    <img src={logo} alt='' height="30px" width='auto'/>
                    <h4 style={{display: 'inline-block', margin: '0px', padding: '10px 0px 10px 0px'}}>Sveikatos priežiūros informacinė sistema</h4>
                </div>
                <div className="ui-toolbar-group-right">
                    <NavLink to="/login">
                        <Button id='login' className="login-button" style={{margin: '5px 0px 0px 0px'}}>
                            <span className="glyphicon glyphicon-log-in" aria-hidden="true" style={{marginRight: '5px'}}></span>
                            Prisijungimas
                        </Button>
                    </NavLink>               
                </div>
            </Toolbar>
            <AppointmentsStatisticsContainer />
            <PrescriptionsStatisticsContainer />
            <FooterComponent />
        </div>
    );
};

export default HomepageComponent;