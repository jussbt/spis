import React, { Component } from 'react';
import axios from '../../node_modules/axios';
import { AllPatientsConponent } from './AllPatientsComponent';
import { InputText } from 'primereact/components/inputtext/InputText';
import { Button } from 'primereact/components/button/Button';
import { Messages } from 'primereact/components/messages/Messages';
export class AllPAtientsConteiner extends Component {
    constructor() {
        super();
        this.state = {
            filters: {},
            patients: [],
            doctorData: [],
            dialogBoxViable: false,
            doctorId: 0,
            columnData: null,
            afterChangePatientToShow: 0,
            findPatientValue: 0
        };
    }

    componentDidMount = () => {

        axios.get('http://localhost:8080/api/admin/finduserById/' + this.props.match.params.userId)
            .then((response) => {
                this.setState({
                    doctorData: response.data
                });
            }
            );
    }

    buttonClick = (rowData) => {
        this.setState({

            visible: true,
            columnData: rowData
        });
       
    }

    onHide = () => {
        this.setState({
            visible: false,
            columnData: null
        });
    }

    actionTemplate = (rowData, column) => {
        return <div>
            <Button label="Priskirti " icon="fa-plus" className="ui-button-secondary" onClick={() => this.buttonClick(rowData)} id="ascribe" />
        </div>;
    }

    changeDoctor = () => {      
        axios.post('http://localhost:8080/api/admin/changeDoctor/' + this.state.doctorData.userId + '/' + this.state.columnData.uuid)
            .then(() => {
                this.setState({
                    patients: [],
                    visible: false,               
                });
          
                this.messages.show({ severity: 'success', summary: "Pacientas buvo priskirtas sėkmingai", life: 5000 });

            }
            );
    }

    findPatient = () => {   
        if(this.state.findPatientValue==0){
            this.messages.show({ severity: 'error', summary: 'Neįvestas asmens kodas', life: 5000 }); 
            return;
        }    
        if(this.state.findPatientValue.length<11){
            this.messages.show({ severity: 'error', summary: 'Asmens kodas - 11 skaitmenų', life: 5000 }); 
            return;
        }    
        axios.get("http://localhost:8080/api/admin/findpatients/" + this.state.findPatientValue)
            .then(response => {
                if(response.data.length==0){
                    this.messages.show({ severity: 'error', summary: 'Pacientas nerastas', life: 5000 }); 
                }  else{ 
                    this.setState({
                        patients: response.data
                    });
                }
            })
            .catch(() => {
                
            });
    }

    render() {

        let bodyMessages =
            <div>
                <Messages ref={(el) => { this.messages = el; }} style={{margin: '0 auto'}}></Messages>
            </div>;
        let dialogfooter =
            <div>
                <Button label="Taip" icon="fa-check" onClick={this.changeDoctor} />
                <Button label="Ne" icon="fa-close" onClick={this.onHide} />
            </div>;

        var header = <div style={{ 'textAlign': 'left' }}>
            <i className="fa fa-search" style={{ margin: '4px 4px 0 0' }}></i>
            <InputText type="search" onInput={(e) => this.setState({ findPatientValue: e.target.value })} placeholder="Paciento paieška pagal asmens kodą" size="50" id="searchPacient" keyfilter="int" maxlength="11"/>
            <Button label="Surasti " onClick={this.findPatient} style={{marginLeft: '10px'}}/>
        </div>;

        return (
            <div>
                <AllPatientsConponent
                    patients={this.state.patients}
                    header={header}
                    globalFilter={this.state.globalFilter}
                    items={this.items}
                    body={this.actionTemplate}
                    visible={this.state.visible}
                    dialogfooter={dialogfooter}
                    onHide={this.onHide}
                    doctor={this.state.doctorData.firstName + " " + this.state.doctorData.lastName}
                    successMessage={this.state.successMessage}
                    history={this.props.history}
                    bodyMessages={bodyMessages}
                />
            </div>
        );
    }
}