import React from 'react';
import { AdminNavigation } from '../Admin/AdminNavigation';
import { DataTable } from 'primereact/components/datatable/DataTable';
import { Column } from 'primereact/components/column/Column';
import { Dialog } from 'primereact/components/dialog/Dialog';

export var AllPatientsConponent = (props) => {

    const sucessStyle = {
        color: 'green'
    };

    const textAlignStyle = {
        textAlign: 'center'
    };

    return (
        <div>
            <AdminNavigation history={props.history} />
            <Dialog header="Patvirtinkite" visible={props.visible} width={'350px'} modal="true" footer={props.dialogfooter} onHide={props.onHide}>
                    Ar tikrai norite priskirti gydytojui {props.doctor} ? </Dialog>
            <div className="content-section introduction">
                <DataTable  value={props.patients} header={props.header}
                    globalFilter={props.globalFilter} emptyMessage="Įrašų nėra">
                    <Column style={textAlignStyle} field="firstName" header="Vardas" />
                    <Column style={textAlignStyle} field="lastName" header="Pavardė" />
                    <Column style={textAlignStyle} field="personalCode" header="Asmens kodas" />

                    <Column style={textAlignStyle} field="assignedDoctorId" header="Dabartinio daktaro ID" />
                    <Column header={"Priskirti"} body={props.body} style={{ textAlign: 'center', width: '10em' }} />
                </DataTable>
                <p style={sucessStyle}> {props.successMessage}</p>
                {props.bodyMessages}
            </div>
        </div>
    );
};




