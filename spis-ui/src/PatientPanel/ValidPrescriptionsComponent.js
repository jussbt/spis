import React from 'react';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRowColumn,
    TableRow
} from 'material-ui/Table'; 
import Dialog from 'material-ui/Dialog';
import Pagination from 'react-js-pagination';

const ValidPrescriptionsComponent = (props) => {
    return (
        <div>
            
            <Table id="validPrescriptionsTable" onRowSelection={props.selectHandler}>

                <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                    <TableRow>
                        <TableHeaderColumn style={{textAlign: 'center'}}>Išrašymo data</TableHeaderColumn>
                        <TableHeaderColumn style={{textAlign: 'center'}}>Veiklioji medžiaga</TableHeaderColumn>
                        <TableHeaderColumn style={{textAlign: 'center'}}>Kiekis</TableHeaderColumn>
                        <TableHeaderColumn style={{textAlign: 'center'}}>Galioja iki</TableHeaderColumn>
                        <TableHeaderColumn style={{textAlign: 'center'}}>Vartojimas</TableHeaderColumn>        
                    </TableRow>
                </TableHeader>

                <TableBody displayRowCheckbox={false} showRowHover={true}>
                    {props.prescriptions}
                </TableBody>
            </Table>
            <Dialog
                id="validPrescriptionsDialog" 
                open={props.dialogOpen} 
                title="Recepto panaudojimų sąrašas"
                onRequestClose={props.dialogClose}
            >
                <b>Veiklioji medžiaga: </b>{props.ingredient}<br />
                <b>Vartojimas: </b>{props.description}
                <Table>
                    <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                        <TableRow>
                            <TableHeaderColumn style={{textAlign: 'center'}}>Eil. Nr.</TableHeaderColumn>
                            <TableHeaderColumn style={{textAlign: 'center'}}>Panaudojimo data</TableHeaderColumn>
                            <TableHeaderColumn style={{textAlign: 'center'}}>Vaistinė</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                        {props.purchases.map( (row, index) => {
                            return (           
                                <TableRow key={index}>
                                    <TableRowColumn style={{textAlign: 'center'}}>{index + 1}</TableRowColumn>
                                    <TableRowColumn style={{textAlign: 'center'}}>{row.date}</TableRowColumn>
                                    <TableRowColumn style={{textAlign: 'center'}}>{row.pharmacist.workplace}</TableRowColumn>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
                
            </Dialog>
            {
                props.totalPages > 1 ? 
                    <div style={{textAlign: 'center'}}>
                        <Pagination
                            id="validPrescriptionsPagination"
                            activePage={props.activePage}
                            totalItemsCount={props.totalItemsCount}
                            onChange={props.onChange}
                        />
                    </div>
                    :
                    <div>

                    </div>
            }
        </div>
    );
};

export default ValidPrescriptionsComponent;