import React from 'react';
import axios from 'axios';
import {TableRow, TableRowColumn} from 'material-ui/Table';
import InvalidPrescriptionsComponent from './InvalidPrescriptionsComponent';

class InvalidPrescriptionsContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            prescriptions: [],
            purchases: [],
            open: false, // indicates whether modal dialog is opened
            activePage: 1,
            totalItemsCount: 0,
            totalPages: 1,
            prescriptionDesc: '',
            prescriptionIngredient: ''
        };
    }

    handleRowSelection = (selectedRow) => {
        const selectedPrescription = this.state.prescriptions[selectedRow[0]].props.data;
        axios.get('http://localhost:8080/api/purchase/' + selectedPrescription.uuid)
            .then(response => {
                this.setState({
                    open: true,
                    purchases: response.data,
                    prescriptionDesc: selectedPrescription.description,
                    prescriptionIngredient: selectedPrescription.drug.activeIngredient
                });
            })
            .catch(() => {

            });
    }

    handleDialogClose = () => {
        this.setState( {open: false} );
    }

    handlePageChange = (pageNumber) => {
        const page = pageNumber - 1; 
        axios.get('http://localhost:8080/api/prescriptions/invalid?page=' + page)
            .then(response => {
                const rows = this.makeTableRows(response.data.content);
                this.setState({
                    activePage: pageNumber,
                    prescriptions: rows
                });
            })
            .catch(() => {

            });
    }

    makeTableRows = (prescriptions) => {
        const rows = prescriptions.map((prescription, index) => {
            const {date, drug: {activeIngredient} = {}, amount, units, expiryDate, description} = prescription;
            return (
                <TableRow key={index} data={prescription}>
                    <TableRowColumn style={{textAlign: 'center'}}>{date}</TableRowColumn>
                    <TableRowColumn style={{textAlign: 'center'}}>{activeIngredient}</TableRowColumn>
                    <TableRowColumn style={{textAlign: 'center'}}>{amount} {units}</TableRowColumn>
                    <TableRowColumn style={{textAlign: 'center'}}>{expiryDate}</TableRowColumn>
                    <TableRowColumn style={{textAlign: 'center'}}>{description}</TableRowColumn>
                </TableRow>
            );
        });
        return rows;
    }

    componentDidMount() {
        axios.get('http://localhost:8080/api/prescriptions/invalid?page=0')
            .then(response => {
                const rows = this.makeTableRows(response.data.content);
                this.setState({
                    prescriptions: rows,
                    totalItemsCount: response.data.totalElements,
                    totalPages: response.data.totalPages,
                    pageRangeDisplayed: response.data.totalPages
                });
            })
            .catch(() => {
                
            });
    }

    render() {
        return (
            <InvalidPrescriptionsComponent
                selectHandler={this.handleRowSelection} 
                dialogOpen={this.state.open}
                dialogClose={this.handleDialogClose}
                prescriptions={this.state.prescriptions}
                history={this.props.history}
                activePage={this.state.activePage}
                totalItemsCount={this.state.totalItemsCount}
                totalPages={this.state.totalPages}
                onChange={this.handlePageChange}
                purchases={this.state.purchases}
                description={this.state.prescriptionDesc}
                ingredient={this.state.prescriptionIngredient}
            />
        );
    }
}

export default InvalidPrescriptionsContainer;