import React from 'react';
import Toggle from 'material-ui/Toggle';
import ValidPrescriptionsContainer from './ValidPrescriptionsContainer';
import InvalidPrescriptionsContainer from './InvalidPrescriptionsContainer';
import NavigationComponent from './NavigationComponent';

class PrescriptionContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            toggled: false, // indicates whether Toggle component is on or off and lets render appropiate prescriptions
        };
    }

    handleToggle = (e, isInputChecked) => {
        this.setState({toggled: isInputChecked});
    }

    render() {
        const styles = {
            block: {
                maxWidth: 300,
                marginBottom: 16,
                marginTop: 32
            }
        };

        const table = this.state.toggled ? <InvalidPrescriptionsContainer /> : <ValidPrescriptionsContainer />;

        return (
            <div>
                <NavigationComponent history={this.props.history}/>
                <Toggle
                    label="Rodyti nebegaliojančius receptus"
                    style={styles.block}
                    onToggle={this.handleToggle}
                />
                {table}
            </div>
        );    
    }
}

export default PrescriptionContainer;