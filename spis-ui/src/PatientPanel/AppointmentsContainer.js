import React from 'react';
import axios from 'axios';
import {TableRow, TableRowColumn} from 'material-ui/Table'; 
import AppointmentsComponent from './AppointmentsComponent';
import NavigationComponent from './NavigationComponent';

class AppointmentsContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            appointments: [],
            appointment: {}, // clicked row appointment
            open: false, // indicates whether modal dialog is opened
            activePage: 1,
            totalItemsCount: 0,
            totalPages: 1
        };
    }

    handleRowSelection = (selectedRows) => {
        const info = this.state.appointments[selectedRows[0]].props.data;
        this.setState({
            appointment: info,
            open: true
        });
    }

    handleDialogClose = () => {
        this.setState({open: false});
    }

    handlePageChange = (pageNumber) => {
        const page = pageNumber - 1; 
        axios.get('http://localhost:8080/api/appointment?page=' + page)
            .then(response => {
                const rows = response.data.content.map((appointment, index) => {
                    const {date, disease, doctor} = appointment;
                    return (
                        <TableRow key={index} data={appointment}>
                            <TableRowColumn style={{textAlign: 'center'}}>{date}</TableRowColumn>
                            <TableRowColumn style={{textAlign: 'center'}}>{disease.code}</TableRowColumn>
                            <TableRowColumn style={{textAlign: 'center'}}>{doctor.firstname}</TableRowColumn>
                            <TableRowColumn style={{textAlign: 'center'}}>{doctor.lastname}</TableRowColumn>
                        </TableRow>
                    );
                });
                this.setState({
                    activePage: pageNumber,
                    appointments: rows
                });
            })
            .catch(() => {
                
            });
    }

    componentDidMount() {
        axios.get('http://localhost:8080/api/appointment?page=0')
            .then(response => {
                const rows = response.data.content.map((appointment, index) => {
                    const {date, disease, doctor} = appointment;
                    return (
                        <TableRow key={index} data={appointment}>
                            <TableRowColumn style={{textAlign: 'center'}}>{date}</TableRowColumn>
                            <TableRowColumn style={{textAlign: 'center'}}>{disease.code}</TableRowColumn>
                            <TableRowColumn style={{textAlign: 'center'}}>{doctor.firstname}</TableRowColumn>
                            <TableRowColumn style={{textAlign: 'center'}}>{doctor.lastname}</TableRowColumn>
                        </TableRow>
                    );
                });
                this.setState({
                    appointments: rows,
                    totalItemsCount: response.data.totalElements,
                    totalPages: response.data.totalPages
                });
            })
            .catch(() => {
                
            });
    }

    render() {
        return (
            <AppointmentsComponent 
                appointments={this.state.appointments}
                appointment={this.state.appointment} 
                selectHandler={this.handleRowSelection} 
                dialogOpen={this.state.open}
                dialogClose={this.handleDialogClose}
                activePage={this.state.activePage}
                totalItemsCount={this.state.totalItemsCount}
                totalPages={this.state.totalPages}
                onChange={this.handlePageChange}
                navigation={<NavigationComponent history={this.props.history}/>}
            />
        );
    }
}

export default AppointmentsContainer;