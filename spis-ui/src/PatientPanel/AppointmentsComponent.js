import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow
} from 'material-ui/Table'; 
import Dialog from 'material-ui/Dialog';
import React from 'react';
import Pagination from 'react-js-pagination';

var AppointmentsComponent = (props) => {
    const {date, duration, description: aprasymas, disease: {description} = {}, isCompensated, isRepeated} = props.appointment;
    const kompensuojamas = isCompensated ? 'Taip' : 'Ne';
    const pakartotinis = isRepeated ? 'Taip' : 'Ne';
    return (
        <div>
            {props.navigation}
            <Table id="appointmentsTable" onRowSelection={props.selectHandler} headerStyle={{marginTop: 30}}>

                <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                    <TableRow>
                        <TableHeaderColumn style={{textAlign: 'center'}}>Vizito data</TableHeaderColumn>
                        <TableHeaderColumn style={{textAlign: 'center'}}>Ligos kodas</TableHeaderColumn>
                        <TableHeaderColumn style={{textAlign: 'center'}}>Gydytojo vardas</TableHeaderColumn>
                        <TableHeaderColumn style={{textAlign: 'center'}}>Gydytojo pavardė</TableHeaderColumn>
                    </TableRow>
                </TableHeader>

                <TableBody displayRowCheckbox={false} showRowHover={true}>
                    {props.appointments}
                </TableBody>
            </Table>
            <Dialog 
                id="appointmentsDialog"
                open={props.dialogOpen} 
                title="Informacija"
                onRequestClose={props.dialogClose}
            >
                <label>Vizito data</label>
                <p>{date}</p>
                <label>Diagnozė</label>
                <p>{description}</p>
                <label>Vizito trukmė</label>
                <p>{duration} min</p>
                <label>Vizito aprašymas</label>
                <p>{aprasymas}</p>
                <label>Kompensuojamas</label>
                <p>{kompensuojamas}</p>
                <label>Pakartotinis</label>
                <p>{pakartotinis}</p>
            </Dialog>
            {
                props.totalPages > 1 ? 
                    <div style={{textAlign: 'center'}}>
                        <Pagination
                            id="appointmentsPagination"
                            activePage={props.activePage}
                            totalItemsCount={props.totalItemsCount}
                            onChange={props.onChange}
                        />
                    </div>
                    :
                    <div>

                    </div>
            } 
        </div>
    );
};

export default AppointmentsComponent;