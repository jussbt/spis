import React from 'react';
import LogOutComponent from '../LoginLogout/LogOutComponent';
import {Toolbar} from 'primereact/components/toolbar/Toolbar';
import {Button} from 'primereact/components/button/Button';
import {NavLink} from 'react-router-dom';
import logo from '../logo.png';

var NavigationComponent = (props) => {
    return (
        <Toolbar>
            <div className="ui-toolbar-group-left">               
                <img src={logo} alt='' height="30px" width='auto'/>             
                <NavLink to="/patient/appointments">
                    <Button id="appointments" label="Ligų įrašai" icon="fa-folder-open" />
                </NavLink>
                <NavLink to="/patient/prescriptions">
                    <Button id="prescriptions" label="Receptai" icon="fa-folder-open" />
                </NavLink>  
            </div>
            <div className="ui-toolbar-group-right">
                <NavLink to="/changepassword">
                    <Button id="password" label="Slaptažodžio keitimas" className="ui-button-warning" />
                </NavLink>
                <LogOutComponent history={props.history}/>
            </div>
        </Toolbar>
    );
};

export default NavigationComponent;