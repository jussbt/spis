import React, { Component } from 'react';
import axios from '../../node_modules/axios';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn, } from 'material-ui/Table';
import PharmacistNavigationComponent from './PharmacistNavigationComponent';
import TextField from 'material-ui/TextField';
import { red500, orange500, blue500 } from 'material-ui/styles/colors';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { Messages } from 'primereact/components/messages/Messages';
import Pagination from 'react-js-pagination';

const styles = {
    errorStyle: {
        color: red500,
    },
    underlineStyle: {
        borderColor: orange500,
    },
    floatingLabelStyle: {
        color: blue500,
    },
    floatingLabelFocusStyle: {
        color: blue500,
    },
};

export default class PharmacistContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            prescriptionsData: [],
            prescription: {},
            fixedHeader: true,
            fixedFooter: true,
            stripedRows: false,
            showRowHover: true,
            selectable: true,
            showCheckboxes: false,
            deselectOnClickaway: false,
            open: false,
            popoverOpen: false,
            totalItemsCount: 0,
            totalPages: 1,
            activePage: 1
        };
    }

    getData = () => {

        if (this.refs.searchBar.getValue().length > 10) {
            axios.get('http://localhost:8080/api/prescriptions/pharmacist/' + this.refs.searchBar.getValue() + '/?page=0')
                .then((response) => {                    
                    if(response.data.content.length==0){
                        this.messages.show({ severity: 'error', summary: 'Pacientas neturi receptu', life: 4000 });
                        
                    }
                    
                    const rows = response.data.content.map((prescription, index) => {
                      
                        const { date, amount, expiryDate, drug, units } = prescription;
                        
                        
                        return (                            
                            <TableRow key={index} data={prescription}>
                                <TableRowColumn style={{ textAlign: 'left' }}>{date}</TableRowColumn>
                                <TableRowColumn style={{ textAlign: 'left' }}>{drug.activeIngredient}</TableRowColumn>
                                <TableRowColumn style={{ textAlign: 'left' }}>{amount} {units}</TableRowColumn>
                                <TableRowColumn style={{ textAlign: 'left' }}>{expiryDate == null ? 'Neterminuotas' : expiryDate}</TableRowColumn>                                
                            </TableRow>
                            
                        );
                        
                    });
                    
                    this.setState({
                        prescriptionsData: rows,
                        totalItemsCount: response.data.totalElements,
                        totalPages: response.data.totalPages
                    });
                    
                  
                   
                }) .catch(error => {
                    if (error.response.status === 404) {
                        this.messages.show({ severity: 'error', summary: 'Pacientas tokiu asmens kodu nerastas', life: 4000 });
                    }
                   

                });
                
        } else {
           
            this.setState({
                prescriptionsData: []
            });
        }
        
    }
    handleToggle = (event, toggled) => {
        this.setState({
            [event.target.name]: toggled,
        });
    };

    handleChange = (event) => {
        this.setState({ height: event.target.value });
    };
    handleRowSelection = (selectedRow) => {
        const info = this.state.prescriptionsData[selectedRow[0]].props.data;
        this.setState({
            prescription: info,
            open: true
        });
    }
    handleOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };
    handleSubmit = (e) => {
        const preId = {
            id: this.state.prescription.uuid
        };
        axios.post("http://localhost:8080/api/purchase/" + preId.id)
            .then(response => {
                if (response.status === 200) {
                    this.setState({ open: false });
                    this.messages.show({ severity: 'success', summary: 'Recepto panaudojimo faktas išsaugotas', life: 4000 });
                    this.cancel(e);
                }
            })
            .catch(error => {
                if (error.response.status === 404) {
                    this.handleClose();
                    this.messages.show({ severity: 'error', summary: 'Įvyko klaida, bandykite dar kartą', life: 4000 });
                }
            });
    }
    
    onChange = (pageNumber) => {
        const page = pageNumber - 1; 
        axios.get('http://localhost:8080/api/prescriptions/pharmacist/' + this.refs.searchBar.getValue() + '/?page=' + page)
            .then(response => {
                const rows = response.data.content.map((prescription, index) => {
                    const { date, amount, expiryDate, drug, units } = prescription;
                    return (
                        <TableRow key={index} data={prescription}>
                            <TableRowColumn style={{ textAlign: 'left' }} >{date}</TableRowColumn>
                            <TableRowColumn style={{ textAlign: 'left' }}>{drug.activeIngredient}</TableRowColumn>
                            <TableRowColumn style={{ textAlign: 'left' }}>{amount} {units}</TableRowColumn>
                            <TableRowColumn style={{ textAlign: 'left' }}>{expiryDate == null ? 'Neterminuotas' : expiryDate}</TableRowColumn>
                        </TableRow>
                    );
                });
                this.setState({
                    activePage: pageNumber,
                    prescriptionsData: rows
                });
            })
            .catch(() => {
                
            });
    }

    render() {
        const actions = [
            <FlatButton
                key="cancelButon"
                label="Atšaukti"
                primary={true}
                onClick={this.handleClose}
            />,
            <FlatButton
                key="submitButon"
                label="Patvirtinti"
                primary={true}
                onClick={this.handleSubmit}
            />,
        ];
        return (
            <div>
                <PharmacistNavigationComponent history={this.props.history} />
                <TextField
                    id="pharmacistSearchBar"
                    ref="searchBar"
                    floatingLabelText="Įveskite asmens kodą"
                    floatingLabelStyle={styles.floatingLabelStyle}
                    floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                    fullWidth={true}
                    onChange={this.getData}
                    maxLength="11"
                />
                <Table
                    onRowSelection={this.handleRowSelection}
                    fixedHeader={this.state.fixedHeader}
                    fixedFooter={this.state.fixedFooter}
                    selectable={this.state.selectable}
                    multiSelectable={this.state.multiSelectable}
                >
                    <TableHeader
                        adjustForCheckbox={this.state.showCheckboxes}
                        displaySelectAll={this.state.showCheckboxes}
                    >
                        <TableRow>
                            <TableHeaderColumn style={{ textAlign: 'left' }}>Išdavimo data</TableHeaderColumn>
                            <TableHeaderColumn style={{ textAlign: 'left' }}>Veiklioji medžiaga</TableHeaderColumn>
                            <TableHeaderColumn style={{ textAlign: 'left' }}>Kiekis</TableHeaderColumn>
                            <TableHeaderColumn style={{ textAlign: 'left' }}>Galioja iki</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody
                        showRowHover={this.state.showRowHover}
                        stripedRows={this.state.stripedRows}
                        displayRowCheckbox={this.state.showCheckboxes}
                    >
                        {this.state.prescriptionsData}
                    </TableBody>
                </Table>
                <Dialog
                    title="Pirkimo patvirtinimas"
                    actions={actions}
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                >
                    Ar tikrai norite patvirtinti recepto panaudojimą?
                </Dialog>
                {
                    this.state.totalPages > 1 ?
                        <div style={{ textAlign: 'center' }}>
                            <Pagination
                                id="appointmentsPagination"
                                activePage={this.state.activePage}
                                totalItemsCount={this.state.totalItemsCount}
                                onChange={this.onChange}
                            />
                        </div>
                        :
                        <div>

                        </div>
                }
                <Messages ref={(el) => { this.messages = el; }} style={{ margin: '0 auto'}}></Messages>
            </div>
        );
    }
}