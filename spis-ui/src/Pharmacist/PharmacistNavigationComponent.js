import React from 'react';
import { NavLink } from 'react-router-dom';
import { Toolbar } from 'primereact/components/toolbar/Toolbar';
import { Button } from 'primereact/components/button/Button';
import LogOutComponent from '../LoginLogout/LogOutComponent';
import logo from '../logo.png';

var PharmacistNavigationComponent = (props) => {

    return (
        <Toolbar>
            <div className="ui-toolbar-group-left">               
                <img src={logo} alt='' height="30px" width='auto'/>                
                <NavLink to="/pharmacist/prescriptions">
                    <Button label="Receptai" icon="fa-folder-open" />
                </NavLink>
            </div>
            <div className="ui-toolbar-group-right">
                <NavLink to="/changepassword">
                    <Button label="Keisti slaptažodį" icon="fa-folder-open" className="ui-button-warning" />
                </NavLink>
                <LogOutComponent history={props.history} />
            </div>
        </Toolbar>
    );
};

export default PharmacistNavigationComponent;