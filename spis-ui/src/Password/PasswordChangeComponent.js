import React from 'react';
import { Password } from 'primereact/components/password/Password';
import { Button } from 'primereact/components/button/Button';
import { Messages } from 'primereact/components/messages/Messages';
import axios from 'axios';
import { AdminNavigation } from '../Admin/AdminNavigation';
import { DoctorNavigationComponent } from '../DoctorPanel/DoctorNavigation/DoctorNavigationComponent';
import NavigationComponent from '../PatientPanel/NavigationComponent';
import PharmacistNavigationComponent from '../Pharmacist/PharmacistNavigationComponent';

class PasswordChangeComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            oldPassword: '',
            newPassword: '',
            newPasswordRepeat: '',
            passwordErrorMessage:'',
            buttonActivationConditions:true
        };
    }

    getNavComponent = () => {
        var component = {};
        switch (atob(localStorage.getItem('UserRole'))) {
        case 'kaspa':
            component = <AdminNavigation history={this.props.history}/>;
            break;
        case 'algis':
            component = <NavigationComponent history={this.props.history}/>;
            break;
        case 'justas':
            component = <PharmacistNavigationComponent history={this.props.history}/>;
            break;
        case 'ignas':
            component = <DoctorNavigationComponent history={this.props.history}/>;
            break;
        default:
            break;
        }
        return component;
    }

    changePassword = (event) => {
        event.preventDefault();
        if (this.state.oldPassword === "" || this.state.newPassword.is === "" ||
            this.state.newPasswordRepeat === "") {
            this.setState({
                passwordErrorMessage: "Prašome užpildyti visus laukus"
            });
            return;
        }
        if (this.state.newPassword.length < 5){
            this.setState({
                passwordErrorMessage: "Naujasis slaptažodis turėtų būti bent 5 simboliai"
            });
            return;
        }

        if (this.state.newPassword !== this.state.newPasswordRepeat) {
            this.setState({
                passwordErrorMessage: "Slaptažodžiai nesutampa"
            });
            return;
        }

        if (this.state.newPassword === this.state.oldPassword) {
            this.setState({
                passwordErrorMessage: "Negalima keisti į seną slaptažodį"
            });
            return;
        }
        
        const password = {
            oldPassword: this.state.oldPassword,
            newPassword: this.state.newPassword
        };
        
        axios.put('http://localhost:8080/api/changepassword', password)
            .then(response => {
                if (response.status === 200) {
                    this.setState({
                        passwordErrorMessage: '',
                        oldPassword: '',
                        newPassword: '',
                        newPasswordRepeat: ''
                    });
                    this.messages.show({ severity: 'success', summary: 'Slaptažodis sėkmingai atnaujintas', life: 5000 });
                } else {
                    this.setState({
                        passwordErrorMessage: "Neteisingas senas slaptažodis"
                    });
                }
            });
    }

    render() {

        return (
            <div>
                {this.getNavComponent()}
                <form className="form-horizontal" onSubmit={this.changePassword} onChange={this.handleChangeForm} style={{marginTop: 30}}>

                    <div className="form-group">
                        <label className="col-sm-5 control-label">Dabartinis slaptažodis</label>
                        <div className="col-sm-5">
                            <Password feedback={false} value={this.state.oldPassword} onChange={(e) => this.setState({ oldPassword: e.target.value })} />
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="col-sm-5 control-label">Naujas slaptažodis</label>
                        <div className="col-sm-5">
                            <Password feedback={false} value={this.state.newPassword} onChange={(e) => this.setState({ newPassword: e.target.value })} />
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="col-sm-5 control-label">Pakartoti naują slaptažodį</label>
                        <div className="col-sm-5">
                            <Password  feedback={false} value={this.state.newPasswordRepeat} onChange={(e) => this.setState({ newPasswordRepeat: e.target.value })} /><br /><br />
                            <Button  id="password" disabled = {!this.state.buttonActivationConditions} label="Pakeisti"  className="ui-button-success" />
                        </div>
                    </div>

                    <div className="form-group">
                        <div className="col-sm-offset-5 col-sm-5">
                            <p style={{color: 'red'}}>{this.state.passwordErrorMessage}</p>
                        </div>
                    </div>
                    <Messages ref={(el) => { this.messages = el; }} style={{width: '50%', margin: 'auto'}}></Messages>         
                </form>
            </div>
        );
    }
}

export default PasswordChangeComponent;
